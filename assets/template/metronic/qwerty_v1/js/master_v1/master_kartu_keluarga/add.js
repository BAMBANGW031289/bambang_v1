$(document).ready(function () {
    const form = document.getElementById('add_master_kartu_keluarga')

    var validator = FormValidation.formValidation(form, {
        fields: {
            id_users_profile: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi id_users_profile'
                    }
                }
            },
            nomor_kartu_keluarga: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi nomor_kartu_keluarga'
                    }
                }
            },
            alamat_rumah: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi alamat_rumah'
                    }
                }
            },
            kode_pos: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi kode_pos'
                    }
                }
            },
        },

        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: '.fv-row',
                eleInvalidClass: '',
                eleValidClass: ''
            })
        }
    })

    $('#add_master_kartu_keluarga').submit(function (e) {
        e.preventDefault()
        if (validator) {
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    $('#kt_sign_in_submit').attr('data-kt-indicator', 'on')
                    $('#kt_sign_in_submit').attr('disabled', true)

                    $.ajax({
                        url: `${target}/add_process`,
                        type: 'post',
                        data: $('#add_master_kartu_keluarga').serialize(),
                        dataType: 'json',
                        success: data => {
                            alert(data.message)
                            $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                            $('#kt_sign_in_submit').removeAttr('disabled')
                            if (data.status == 200) {
                                window.location.replace(`${target}`)
                            }
                        },
                        error: e => {
                            alert(`${e.status} - ${e.statusText}`)
                            $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                            $('#kt_sign_in_submit').removeAttr('disabled')
                        }
                    })
                }
            })
        }
    })
})
