$(document).ready(function () {
    const form = document.getElementById('edit_master_kartu_keluarga')

    var validator = FormValidation.formValidation(form, {
        fields: {
            nomor_kartu_keluarga: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi nama edit_master_kartu_keluarga'
                    }
                }
            },
        },

        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: '.fv-row',
                eleInvalidClass: '',
                eleValidClass: ''
            })
        }
    })

    $('#edit_master_kartu_keluarga').submit(function (e) {
        e.preventDefault()
        if (validator) {
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    $('#kt_sign_in_submit').attr('data-kt-indicator', 'on')
                    $('#kt_sign_in_submit').attr('disabled', true)

                    $.ajax({
                        url: `${target}/edit_process`,
                        type: 'POST',
                        data: $('#edit_master_kartu_keluarga').serialize(),
                        dataType: 'json',
                        success: data => {
                            alert(data.message)
                            $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                            $('#kt_sign_in_submit').removeAttr('disabled')
                            if (data.status == 200) {
                                window.location.replace(`${target}`)
                            }
                        },
                        error: e => {
                            alert(`${e.status} - ${e.statusText}`)
                            $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                            $('#kt_sign_in_submit').removeAttr('disabled')
                        }
                    })
                }
            })
        }
    })
})
