$(document).ready(function () {
    const form = document.getElementById('add_master_hr')

    var validator = FormValidation.formValidation(form, {
        fields: {
            no_rm: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi nomor RM'
                    }
                }
            },
            profesi: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi profesi'
                    }
                }
            },
            nomor_induk_karyawan: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi nomor_induk_karyawan'
                    }
                }
            },
            bpjs_kode_dokter: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi bpjs_kode_dokter'
                    }
                }
            },
            full_name: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi Full Name'
                    }
                }
            },
            username: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi username'
                    }
                }
            },
            kode_karyawan_fingerprint: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi kode_karyawan_fingerprint'
                    }
                }
            },
        },

        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: '.fv-row',
                eleInvalidClass: '',
                eleValidClass: ''
            })
        }
    })

    $('#add_master_hr').submit(function (e) {
        e.preventDefault()
        if (validator) {
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    $('#kt_sign_in_submit').attr('data-kt-indicator', 'on')
                    $('#kt_sign_in_submit').attr('disabled', true)

                    $.ajax({
                        url: `${target}/add_process`,
                        type: 'POST',
                        data: $('#add_master_hr').serialize(),
                        dataType: 'json',
                        success: data => {
                            alert(data.message)
                            $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                            $('#kt_sign_in_submit').removeAttr('disabled')
                            if (data.status == 200) {
                                window.location.replace(`${target}`)
                            }
                        },
                        error: e => {
                            alert(`${e.status} - ${e.statusText}`)
                            $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                            $('#kt_sign_in_submit').removeAttr('disabled')
                        }
                    })
                }
            })
        }
    })
})



