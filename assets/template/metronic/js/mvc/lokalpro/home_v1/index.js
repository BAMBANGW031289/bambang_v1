showOverlay("Proses menampilkan data . . .");

$(document).ready(function () {
  
  var table = $("#table_users").DataTable({
		columnDefs: [
			{
				targets: [0, 1],
				orderable: false,
			}
		],
		language: {
			lengthMenu: "Tampilkan _MENU_ data",
			zeroRecords: "Tidak ada data untuk ditampilkan.",
			info: "Menampilkan _START_ - _END_ dari _TOTAL_ data",
			infoEmpty: "Informasi belum tersedia.",
			infoFiltered: "(difilter dari _MAX_ total data)",
		},
		oLanguage: {
			sSearch: "Cari:",
		},

		dom:
			"<'row'" +
			"<'col-12 col-sm-6 d-flex align-items-center justify-content-center justify-content-sm-start'l>" +
			"<'col-12 col-sm-6 d-flex align-items-center justify-content-center justify-content-sm-end'f>" +
			">" +
			"<'table-responsive'tr>" +
			"<'row'" +
			"<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
			"<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
			">",

		scrollX: true,
		processing: false,
		width: "100%",
		drawCallback: function (settings) {
			hideOverlay();
		},
	});

	table
		.on("order.dt search.dt", function () {
			let i = 1;

			table
				.cells(null, 0, { search: "applied", order: "applied" })
				.every(function (cell) {
					this.data(i++ + ".");
				});
		})
		.draw();
});