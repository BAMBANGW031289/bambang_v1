# ************************************************************
# Sequel Ace SQL dump
# Version 20062
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: aws881.cdxpetc2exbn.ap-southeast-3.rds.amazonaws.com (MySQL 8.0.35)
# Database: dev_eka
# Generation Time: 2024-01-17 07:15:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table hr
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hr`;

CREATE TABLE `hr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `no_rm` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `profesi` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT 'role di table hr_role_group',
  `profesi_jabatan` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'STAFF',
  `role_prefix` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `full_name` varchar(512) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `pin` varchar(512) DEFAULT NULL,
  `prefix_antrean` varchar(5) DEFAULT NULL,
  `akses_navbar_level1` varchar(512) DEFAULT NULL COMMENT 'diisi oleh id navbarl_leve1 untuk akses tambahan',
  `bpjs_kode_dokter` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pin_renewal` datetime DEFAULT '2024-01-01 00:00:01',
  `job_start` datetime DEFAULT CURRENT_TIMESTAMP,
  `job_end` datetime DEFAULT NULL,
  `tipe_jadwal` enum('SHIFT','TETAP') DEFAULT NULL,
  `nomor_induk_karyawan` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kode_karyawan_fingerprint` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `npwp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_bank` varchar(50) DEFAULT NULL,
  `no_rekening` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `no_ktp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_satuan_kerja` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `eselon` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `golongan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nomor_induk_karyawan_pasangan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `no_telp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jumlah_anak` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `satusehat_id_practitioner` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `akses` enum('ON','OFF') DEFAULT 'ON',
  `gaji_bulanan` double DEFAULT NULL,
  `str` varchar(256) DEFAULT NULL,
  `str_exp_date` date DEFAULT NULL,
  `sip` varchar(256) DEFAULT NULL,
  `sip_exp_date` date DEFAULT NULL,
  `ktk` varchar(100) DEFAULT NULL,
  `group_unit_kerja` varchar(50) DEFAULT NULL,
  `unit_kerja` varchar(100) DEFAULT NULL,
  `jenis_karyawan` varchar(256) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `pendidikan` varchar(100) DEFAULT NULL,
  `nama_universitas` varchar(512) DEFAULT NULL,
  `status_karyawan` varchar(100) DEFAULT NULL,
  `gaji_bruto` double DEFAULT NULL,
  `tunjangan_pulsa` double DEFAULT NULL,
  `tunjangan_bbm` double DEFAULT NULL,
  `tunjangan_sekretaris` double DEFAULT NULL,
  `tunjangan_makeup` double DEFAULT NULL,
  `tunjangan_internal_memo` double DEFAULT NULL,
  `tunjangan_mengelola_data` double DEFAULT NULL,
  `tunjangan_legal` double DEFAULT NULL,
  `tunjangan_marketing` double DEFAULT NULL,
  `tunjangan_resiko` double DEFAULT NULL,
  `tunjangan_sip` double DEFAULT NULL,
  `tunjangan_casemix` double DEFAULT NULL,
  `tunjangan_makan` double DEFAULT NULL,
  `tunjangan_transportasi` double DEFAULT NULL,
  `tunjangan_kinerja` double DEFAULT NULL,
  `tunjangan_lain_1` double DEFAULT NULL,
  `tunjangan_lain_2` double DEFAULT NULL,
  `jpk_4` double DEFAULT NULL,
  `jkk_jkn_054` double DEFAULT NULL,
  `jpk` double DEFAULT NULL,
  `jkk` double DEFAULT NULL,
  `jkm` double DEFAULT NULL,
  `gaji_total` double DEFAULT NULL,
  `biaya_jabatan` double DEFAULT NULL,
  `iuran_jht` double DEFAULT NULL,
  `iuran_jht_2` double DEFAULT NULL,
  `pengurangan_total` double DEFAULT NULL,
  `ph_netto_bulan` double DEFAULT NULL,
  `ph_netto_tahun` double DEFAULT NULL,
  `ptkp` double DEFAULT NULL,
  `pkp` double DEFAULT NULL,
  `pajak_5` double DEFAULT NULL,
  `pajak_15` double DEFAULT NULL,
  `pajak_25` double DEFAULT NULL,
  `pajak_30` double DEFAULT NULL,
  `pajak_35` decimal(10,0) DEFAULT NULL,
  `pph_21_bulan` double DEFAULT NULL,
  `pph_21_tahun` double DEFAULT NULL,
  `pph` double DEFAULT NULL,
  `gaji_bersih` double DEFAULT NULL,
  `bon_sementara` double DEFAULT NULL,
  `seragam` double DEFAULT NULL,
  `jaminan_hari_tua` double DEFAULT NULL,
  `jaminan_hari_tua_2` double DEFAULT NULL,
  `rawat_inap` double DEFAULT NULL,
  `apotek` double DEFAULT NULL,
  `jaminan_pensiun` double DEFAULT NULL,
  `jaminan_pensiun_2` double DEFAULT NULL,
  `koperasi` double DEFAULT NULL,
  `ikm` double DEFAULT NULL,
  `brk` double DEFAULT NULL,
  `bpjs_kesehatan` double DEFAULT NULL,
  `biaya_adm_bank` double DEFAULT NULL,
  `gaji_netto` double DEFAULT NULL,
  `prestasi` double DEFAULT NULL,
  `jasa_medis` double DEFAULT NULL,
  `contract_details` longtext,
  `kontrak_isi` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `kontrak_ttd_karyawan` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `kontrak_ttd_hrd` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `kontrak_nama_hrd` varchar(100) DEFAULT NULL,
  `kontrak_datetime_karyawan` datetime DEFAULT NULL,
  `kontrak_datetime_hrd` datetime DEFAULT NULL,
  `no_sk` varchar(256) DEFAULT NULL,
  `tanggal_sk` date DEFAULT NULL,
  `tanggal_mutasi` date DEFAULT NULL,
  `prefix` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `alasan_resign` text,
  `status_berhenti_kerja` enum('','RESIGN','FIRED') DEFAULT NULL,
  `unit` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `persen_jasmed_tarif_khusus` float DEFAULT NULL,
  `spesialisasi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nomor_induk_karyawan` (`nomor_induk_karyawan`),
  UNIQUE KEY `nomor_induk_karyawan_2` (`nomor_induk_karyawan`),
  UNIQUE KEY `bpjs_kode_dokter` (`bpjs_kode_dokter`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `kode_karyawan_fingerprint` (`kode_karyawan_fingerprint`) USING BTREE,
  KEY `no_rm` (`no_rm`),
  KEY `role_prefix` (`role_prefix`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hr` WRITE;
/*!40000 ALTER TABLE `hr` DISABLE KEYS */;

INSERT INTO `hr` (`id`, `no_rm`, `profesi`, `profesi_jabatan`, `role_prefix`, `full_name`, `username`, `pin`, `prefix_antrean`, `akses_navbar_level1`, `bpjs_kode_dokter`, `pin_renewal`, `job_start`, `job_end`, `tipe_jadwal`, `nomor_induk_karyawan`, `kode_karyawan_fingerprint`, `npwp`, `nama_bank`, `no_rekening`, `no_ktp`, `nama_satuan_kerja`, `eselon`, `golongan`, `nomor_induk_karyawan_pasangan`, `no_telp`, `jumlah_anak`, `satusehat_id_practitioner`, `status`, `akses`, `gaji_bulanan`, `str`, `str_exp_date`, `sip`, `sip_exp_date`, `ktk`, `group_unit_kerja`, `unit_kerja`, `jenis_karyawan`, `jabatan`, `pendidikan`, `nama_universitas`, `status_karyawan`, `gaji_bruto`, `tunjangan_pulsa`, `tunjangan_bbm`, `tunjangan_sekretaris`, `tunjangan_makeup`, `tunjangan_internal_memo`, `tunjangan_mengelola_data`, `tunjangan_legal`, `tunjangan_marketing`, `tunjangan_resiko`, `tunjangan_sip`, `tunjangan_casemix`, `tunjangan_makan`, `tunjangan_transportasi`, `tunjangan_kinerja`, `tunjangan_lain_1`, `tunjangan_lain_2`, `jpk_4`, `jkk_jkn_054`, `jpk`, `jkk`, `jkm`, `gaji_total`, `biaya_jabatan`, `iuran_jht`, `iuran_jht_2`, `pengurangan_total`, `ph_netto_bulan`, `ph_netto_tahun`, `ptkp`, `pkp`, `pajak_5`, `pajak_15`, `pajak_25`, `pajak_30`, `pajak_35`, `pph_21_bulan`, `pph_21_tahun`, `pph`, `gaji_bersih`, `bon_sementara`, `seragam`, `jaminan_hari_tua`, `jaminan_hari_tua_2`, `rawat_inap`, `apotek`, `jaminan_pensiun`, `jaminan_pensiun_2`, `koperasi`, `ikm`, `brk`, `bpjs_kesehatan`, `biaya_adm_bank`, `gaji_netto`, `prestasi`, `jasa_medis`, `contract_details`, `kontrak_isi`, `kontrak_ttd_karyawan`, `kontrak_ttd_hrd`, `kontrak_nama_hrd`, `kontrak_datetime_karyawan`, `kontrak_datetime_hrd`, `no_sk`, `tanggal_sk`, `tanggal_mutasi`, `prefix`, `alasan_resign`, `status_berhenti_kerja`, `unit`, `persen_jasmed_tarif_khusus`, `spesialisasi`)
VALUES
	(3666,'335235','ST','SATPAM','E','Bud Gun','budigun123',NULL,NULL,NULL,NULL,'2024-01-01 00:00:01','2020-07-28 10:10:00',NULL,'TETAP','2356241','54328532',NULL,NULL,NULL,NULL,'414285',NULL,NULL,NULL,'08098232323',NULL,NULL,NULL,'ON',NULL,NULL,NULL,NULL,NULL,'TK/0','','','Pegawai Kontrak',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,185675,11141,13926,210742,10538,92838,171749,103376,107366,1288392,NULL,1288392,77304,NULL,NULL,NULL,NULL,6442,77304,6442,NULL,NULL,NULL,92838,171749,NULL,NULL,46419,92838,NULL,NULL,NULL,46419,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3672,'777890','SP','SUPIR','G','Com C','tomcrusie12',NULL,NULL,NULL,NULL,'2024-01-01 00:00:01','2023-08-15 10:40:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ON',NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,'',NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,185675,0,0,185675,9284,0,0,9284,176391,2116692,0,2116692,127002,0,0,0,0,10584,127002,10584,0,0,0,0,0,0,0,0,0,0,0,0,46419,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `hr` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kamar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kamar`;

CREATE TABLE `kamar` (
  `id_kamar` int NOT NULL AUTO_INCREMENT,
  `id_departemen` int DEFAULT NULL,
  `kapasitas` int DEFAULT NULL,
  `tersediapriawanita` int DEFAULT NULL,
  `id_kamar_ref_kelas` int DEFAULT NULL COMMENT 'id_ref_aplicare_kode_kelas',
  `id_ref_siranap_kode_kelas_perawatan` int DEFAULT NULL,
  `id_ref_siranap_kode_ruang_perawatan` int DEFAULT NULL,
  `id_ref_rsonline_fasyankes` int DEFAULT NULL,
  `perawatan` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `kode_siranap_kelas_perawatan` varchar(4) DEFAULT NULL,
  `kode_siranap_ruang_perawatan` varchar(4) DEFAULT NULL,
  `dinkes_jenis_bed` enum('TIDAK ADA','vip','kelas_1','kelas_2','kelas_3','hcu','iccu','icu_negatif_ventilator','icu_negatif_tanpa_ventilator','icu_tanpa_negatif_ventilator','icu_tanpa_negatif_tanpa_ventilator','isolasi_negatif','isolasi_tanpa_negatif','nicu_covid','perina_covid','picu_covid','ok_covid','hd_covid','icu_covid_negatif_ventilator','icu_covid_negatif_tanpa_ventilator','icu_covid_tanpa_negatif_ventilator','icu_covid_tanpa_negatif_tanpa_ventilator') DEFAULT NULL,
  `tersediapria` int DEFAULT NULL,
  `tersediawanita` int DEFAULT NULL,
  `is_del` enum('0','1') DEFAULT '0',
  `del_date` datetime DEFAULT NULL,
  `del_by` varchar(256) DEFAULT NULL,
  `id_user` int DEFAULT NULL,
  `is_bor` tinyint DEFAULT '1',
  `is_covid` enum('TRUE','FALSE') DEFAULT 'FALSE',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_kamar`),
  KEY `id_departemen` (`id_departemen`),
  KEY `idx_id_kamar_ref_kelas` (`id_kamar_ref_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

LOCK TABLES `kamar` WRITE;
/*!40000 ALTER TABLE `kamar` DISABLE KEYS */;

INSERT INTO `kamar` (`id_kamar`, `id_departemen`, `kapasitas`, `tersediapriawanita`, `id_kamar_ref_kelas`, `id_ref_siranap_kode_kelas_perawatan`, `id_ref_siranap_kode_ruang_perawatan`, `id_ref_rsonline_fasyankes`, `perawatan`, `kelas`, `kode_siranap_kelas_perawatan`, `kode_siranap_ruang_perawatan`, `dinkes_jenis_bed`, `tersediapria`, `tersediawanita`, `is_del`, `del_date`, `del_by`, `id_user`, `is_bor`, `is_covid`, `created_date`, `created_by`)
VALUES
	(20,430,10,10,16,7,23,12,'Isolasi Imunitas menurun','Isolasi','0007','0022','',NULL,NULL,'0',NULL,NULL,NULL,1,'','2024-01-04 16:29:08','NUDI'),
	(21,400,1,0,8,8,25,6,'ICU/RICU','Rawat Khusus','0008','0024','',1,0,'0',NULL,NULL,NULL,1,'','2024-01-04 16:29:08','TONO');

/*!40000 ALTER TABLE `kamar` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kartu_keluarga
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kartu_keluarga`;

CREATE TABLE `kartu_keluarga` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_users_profile` int DEFAULT NULL,
  `nomor_kartu_keluarga` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `alamat_rumah` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `jakarta_bandung` tinyint DEFAULT NULL COMMENT '1=jakarta, 2=bandung',
  `simpat_proxl` enum('SIMPATI','XL') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `kartu_keluarga` WRITE;
/*!40000 ALTER TABLE `kartu_keluarga` DISABLE KEYS */;

INSERT INTO `kartu_keluarga` (`id`, `id_users_profile`, `nomor_kartu_keluarga`, `alamat_rumah`, `kode_pos`, `created_date`, `jakarta_bandung`, `simpat_proxl`)
VALUES
	(3,1,NULL,'Jl sawo','89191','2023-12-07 00:00:00',1,'SIMPATI'),
	(4,1,'9090821010101','Jl sawo','89191','2023-12-07 00:00:00',1,'SIMPATI'),
	(5,1,'9090821010101','Jl sawo','89191','2023-12-07 00:00:00',1,'XL'),
	(6,1,'9090821010101','Jl sawo','89191','2023-12-07 00:00:00',2,'XL'),
	(8,2147483647,'9090821010101','Jl Semangka','89191',NULL,1,'SIMPATI'),
	(19,45454,NULL,'4444',NULL,NULL,1,'SIMPATI'),
	(21,2147483647,NULL,'Jl. Logawa no 12',NULL,NULL,1,'SIMPATI');

/*!40000 ALTER TABLE `kartu_keluarga` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table log_email_sent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log_email_sent`;

CREATE TABLE `log_email_sent` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tipe_email_sent` enum('MCU','RAD','LAB','PO','PRODUK_EXPIRED','HRD') DEFAULT NULL,
  `no_ref` varchar(50) DEFAULT NULL,
  `email_sent_to` varchar(256) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL COMMENT 'TGL EMAIL TERKIRIM',
  `created_by` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

LOCK TABLES `log_email_sent` WRITE;
/*!40000 ALTER TABLE `log_email_sent` DISABLE KEYS */;

INSERT INTO `log_email_sent` (`id`, `tipe_email_sent`, `no_ref`, `email_sent_to`, `created_date`, `created_by`)
VALUES
	(83,'RAD','RAD/1942/2023/06/27/9/1','testing@gmail.com','2023-06-27 11:03:50','bagus'),
	(84,'LAB','LAB/2046/2023/07/06/31/6','testing5@gmail.com','2023-07-06 10:26:14','toni'),
	(85,'PO','PO/44/2023/08/19/1','1testing@gmail.com','2023-08-19 09:48:51','budi'),
	(86,'RAD','RAD/1111/2023/08/25/67/1','tsdsdesting@gmail.com','2023-08-25 09:48:53','anna');

/*!40000 ALTER TABLE `log_email_sent` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
