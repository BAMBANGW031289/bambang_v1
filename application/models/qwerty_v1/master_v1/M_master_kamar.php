<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_master_kamar extends CI_Model {

	public function __construct()
	{
		parent::__construct();
				
	}
		
    public function get($filter)
    {
        $filter['column_order'] = [];
        $filter['column_search'] = [];
        $filter['order'] = ['a.id_kamar' => 'DESC'];

        $this->db->select('
			a.*
        ');
        $this->db->from('kamar a');
        //$this->db->where('a.id_kamar_ref_master_tipe', $this->id_kamar_ref_master_tipe);
        if (!empty($filter['id_kamar'])) {
            $this->db->where('id_kamar', $filter['id_kamar']);
        }
        if (!isset($filter['offset'])) {
            $filter['offset'] = '0';
        }
        if (isset($filter['limit']) && $filter['limit'] > 0) {
            $this->db->limit($filter['limit'], $filter['offset']);
        }
        $this->_get_datatables_query($filter);
        $this->db->group_by('a.id_kamar');
        $query = $this->db->get()->result_array();
        $data['result'] = $query;

        if (empty($filter['id_kamar'])) {
            $data['record_total'] = $this->_getTotal($filter);
            $data['record_filter'] = $this->_getFilterl($filter);
        }

        $res['status'] = '200';
        $res['message'] = 'Berhasil mendapatkan data';
        $res['data']    = $data;
        return $res;
    }

    private function _get_datatables_query($filter)
    {
        $i = 0;
        if (isset($filter['search']) && $filter['search'] != null) {
            $this->db->group_start();
            foreach ($filter['column_search'] as $item) {
                if ($i == 0) {
                    $this->db->like($item, $filter['search']);
                } else {
                    $this->db->or_like($item, $filter['search']);
                }
                $i++;
            }
            $this->db->group_end();
        }

        if (isset($filter['order_column'])) {
            $this->db->order_by($filter['column_order'][$filter['order_column']], $filter['order_dir']);
        }
        if (isset($filter['order'])) {
            $this->db->order_by(key($filter['order']), $filter['order'][key($filter['order'])]);
        }
    }

    private function _getTotal($filter)
    {
        $this->db->select('
            a.id_kamar id_kamar,
        ');
        $this->db->from('kamar a');
       // $this->db->where('a.id_kamar_ref_master_tipe', $this->id_kamar_ref_master_tipe);
        $this->db->group_by('a.id_kamar');
        return $this->db->get()->num_rows();
    }

    private function _getFilterl($filter)
    {
        $this->db->select('
            a.id_kamar id_kamar,
        ');
        $this->db->from('kamar a');
       // $this->db->where('a.id_kamar_ref_master_tipe', $this->id_kamar_ref_master_tipe);
        $this->_get_datatables_query($filter);
        $this->db->group_by('a.id_kamar');
        return $this->db->get()->num_rows();
    }

    public function add($params)
    {
        $this->db->insert('kamar', $params);
        $id = $this->db->insert_id();
        if ($id) {
            $res['status'] = 200;
            $res['message'] = 'Berhasil tambah data';
            $res['data'] = [
                'id_kamar' => $id
            ];
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal tambah data';
        }
        return $res;
    }

    public function update($id, $data)
    {
        if (empty($id)) {
            $res['status'] = 400;
            $res['message'] = 'Data tidak ditemukan';
        }else{
            $this->db->where("id_kamar", $id);
            $this->db->update("kamar", $data);
                if($this->db->affected_rows() > 0)
                    {
                        $res['status'] = 200;
                        $res['message'] = 'Berhasil update data';
                    }
                else
                    {
                        $res['status'] = 400;
                        $res['message'] = 'Gagal update data.';
                    }
                return $res;
        }
        return $res;
    }

    public function delete($params)
    {
        $this->db->where("id_kamar", $params['id']);
        $this->db->delete("kamar");
         if (empty($params['id'])) {
            $res['status'] = 400;
            $res['message'] = 'Data tidak ditemukan';
        }else{
                if($this->db->affected_rows() > 0)
                    {
                        $res['status'] = 200;
                        $res['message'] = 'Berhasil hapus data';
                    }
                else
                    {
                        $res['status'] = 400;
                        $res['message'] = 'Gagal hapus data.';
                    }
                return $res;
        }
        return $res;
    }
}