<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	public function __construct()
	{
		parent::__construct();
				
	}
		
    public function get($username){
        $this->db->where('username', $username); 
        $result = $this->db->get('user')->row(); 
        return $result;
    }

}