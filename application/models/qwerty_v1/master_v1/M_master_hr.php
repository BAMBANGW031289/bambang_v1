<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_master_hr extends CI_Model {

	public function __construct()
	{
		parent::__construct();
				
	}
		
    public function get($filter)
    {
        $filter['column_order'] = [];
        $filter['column_search'] = [];
        $filter['order'] = ['a.id' => 'DESC'];

        $this->db->select('
			a.*,
        ');
        $this->db->from('hr a');
        if (!empty($filter['id'])) {
            $this->db->where('id', $filter['id']);
        }
        if (!isset($filter['offset'])) {
            $filter['offset'] = '0';
        }
        if (isset($filter['limit']) && $filter['limit'] > 0) {
            $this->db->limit($filter['limit'], $filter['offset']);
        }
        $this->_get_datatables_query($filter);
        $query = $this->db->get()->result_array();
        $data['result'] = $query;

        if (empty($filter['id'])) {
            $data['record_total'] = $this->_getTotal($filter);
            $data['record_filter'] = $this->_getFilterl($filter);
        }

        $res['status'] = '200';
        $res['message'] = 'Berhasil mendapatkan data';
        $res['data']    = $data;
        return $res;
    }

    private function _get_datatables_query($filter)
    {
        $i = 0;
        if (isset($filter['search']) && $filter['search'] != null) {
            $this->db->group_start();
            foreach ($filter['column_search'] as $item) {
                if ($i == 0) {
                    $this->db->like($item, $filter['search']);
                } else {
                    $this->db->or_like($item, $filter['search']);
                }
                $i++;
            }
            $this->db->group_end();
        }

        if (isset($filter['order_column'])) {
            $this->db->order_by($filter['column_order'][$filter['order_column']], $filter['order_dir']);
        }
        if (isset($filter['order'])) {
            $this->db->order_by(key($filter['order']), $filter['order'][key($filter['order'])]);
        }
    }

    private function _getTotal($filter)
    {
        $this->db->select('
            a.id id,
        ');
        $this->db->from('hr a');
       $this->db->where('a.id', $this->id);
        $this->db->group_by('a.id');
        return $this->db->get()->num_rows();
    }

    private function _getFilterl($filter)
    {
        $this->db->select('
            a.id id,
        ');
        $this->db->from('hr a');
        $this->db->where('a.id', $this->id);
        $this->_get_datatables_query($filter);
        $this->db->group_by('a.id');
        return $this->db->get()->num_rows();
    }

    public function add($params)
    {   
       $cek_kd_bpjs_dokter = $this->db->query("SELECT * FROM hr where bpjs_kode_dokter='".$params['bpjs_kode_dokter']."'");
       $cek_kd_karyawan_fingerprint = $this->db->query("SELECT * FROM hr where kode_karyawan_fingerprint='".$params['kode_karyawan_fingerprint']."'");
       $cek_username = $this->db->query("SELECT * FROM hr where username='".$params['username']."'");
       $cek_no_induk_karyawan = $this->db->query("SELECT * FROM hr where nomor_induk_karyawan='".$params['nomor_induk_karyawan']."'");
       if($cek_kd_bpjs_dokter->num_rows() != 0){
            $res['status'] = 400;
            $res['message'] = 'BPJS Kode Dokter sudah ada';
        }elseif($cek_kd_karyawan_fingerprint->num_rows() != 0){
            $res['status'] = 400;
            $res['message'] = 'Kode Fingerprint sudah ada';
        }elseif($cek_username->num_rows() != 0){
            $res['status'] = 400;
            $res['message'] = 'Username sudah ada';
        }elseif($cek_no_induk_karyawan->num_rows() != 0){
            $res['status'] = 400;
            $res['message'] = 'Nomor Induk Karyawan sudah ada';
        }else{   
        $this->db->insert('hr', $params);
        $id = $this->db->insert_id();
        if ($id) {
            $res['status'] = 200;
            $res['message'] = 'Berhasil tambah data';
            $res['data'] = [
                'id' => $id
            ];
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal tambah data';
          }
        }  
        return $res;
    }

    public function update($id, $data)
    {
        if (empty($id)) {
            $res['status'] = 400;
            $res['message'] = 'Data tidak ditemukan';
        }else{
            $this->db->where("id", $id);
            $this->db->update("hr", $data);
                if($this->db->affected_rows() > 0)
                    {
                        $res['status'] = 200;
                        $res['message'] = 'Berhasil update data';
                    }
                else
                    {
                        $res['status'] = 400;
                        $res['message'] = 'Gagal update data.';
                    }
                return $res;
        }
        return $res;
    }

    public function delete($params)
    {
        $this->db->where("id", $params['id']);
        $this->db->delete("hr");
         if (empty($params['id'])) {
            $res['status'] = 400;
            $res['message'] = 'Data tidak ditemukan';
        }else{
                if($this->db->affected_rows() > 0)
                    {
                        $res['status'] = 200;
                        $res['message'] = 'Berhasil hapus data';
                    }
                else
                    {
                        $res['status'] = 400;
                        $res['message'] = 'Gagal hapus data.';
                    }
                return $res;
        }
        return $res;
    }
	
	
}