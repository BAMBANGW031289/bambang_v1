<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


require_once('routes/routes_qwerty_mvc.php');
require_once('routes/routes_qwerty_api.php');
