<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// $route['master_pendidikan'] = 'qwerty_v1/mvc/master_v1/c_master_hr';
// $route['master_pendidikan/(:any)'] = 'qwerty_v1/master_v1/c_master_pendidikan_v1/$1';
// $route['master_hr'] = 'mvc/qwerty_v1/master_v1/c_master_hr';
$route['master_hr'] = 'qwerty_v1/mvc/master_v1/C_master_hr';
$route['master_hr/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_hr/$1';
$route['master_kamar'] = 'qwerty_v1/mvc/master_v1/C_master_kamar';
$route['master_kamar/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_kamar/$1';
$route['master_kartu_keluarga'] = 'qwerty_v1/mvc/master_v1/C_master_kartu_keluarga';
$route['master_kartu_keluarga/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_kartu_keluarga/$1';
$route['master_log_email_sent'] = 'qwerty_v1/mvc/master_v1/C_master_log_email_sent';
$route['master_log_email_sent/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_log_email_sent/$1';
$route['master_pendidikan'] = 'qwerty_v1/mvc/master_v1/C_master_pendidikan_v1';
$route['master_pendidikan/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_pendidikan_v1/$1';