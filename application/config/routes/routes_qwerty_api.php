<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['api/master_hr'] = 'qwerty_v1/api/master_v1/C_master_hr';
$route['api/master_hr/(:any)'] = 'qwerty_v1/api/master_v1/C_master_hr/$1';
$route['api/master_kamar'] = 'qwerty_v1/api/master_v1/C_master_kamar';
$route['api/master_kamar/(:any)'] = 'qwerty_v1/api/master_v1/C_master_kamar/$1';
$route['api/master_kartu_keluarga'] = 'qwerty_v1/api/master_v1/C_master_kartu_keluarga';
$route['api/master_kartu_keluarga/(:any)'] = 'qwerty_v1/api/master_v1/C_master_kartu_keluarga/$1';
$route['api/master_log_email_sent'] = 'qwerty_v1/api/master_v1/C_master_log_email_sent';
$route['api/master_log_email_sent/(:any)'] = 'qwerty_v1/api/master_v1/C_master_log_email_sent/$1';
$route['api/master_pendidikan'] = 'qwerty_v1/api/master_v1/C_master_pendidikan_v1';
$route['api/master_pendidikan/(:any)'] = 'qwerty_v1/api/master_v1/C_master_pendidikan_v1/$1';
//$route['api/master_log_email_sent/update/(:any)'] = 'qwerty_v1/api/master_v1/C_master_log_email_sent/index_put/$1';
