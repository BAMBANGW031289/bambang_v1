<!--begin::Content wrapper-->
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?= $title ?></h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center gap-2 gap-lg-3">

            </div>
            <!--end::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form w-100" novalidate="novalidate" id="add_master_kamar" action="#">
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="id_departemen" name="id_departemen" placeholder="" value="" />
                                                <label for="id_departemen">ID Departemen <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="kapasitas" name="kapasitas" placeholder="" value="" />
                                                <label for="kapasitas">Kapasitas <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="tersediapriawanita" name="tersediapriawanita" placeholder="" value="" />
                                                <label for="tersediapriawanita">tersedia pria wanita <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="id_kamar_ref_kelas" name="id_kamar_ref_kelas" placeholder="" value="" />
                                                <label for="id_kamar_ref_kelas">ID kamar ref kelas <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="id_ref_siranap_kode_kelas_perawatan" name="id_ref_siranap_kode_kelas_perawatan" placeholder="" value="" />
                                                <label for="id_ref_siranap_kode_kelas_perawatan">ID ref siranap kode kelas perawatan <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="id_ref_siranap_kode_ruang_perawatan" name="id_ref_siranap_kode_ruang_perawatan" placeholder="" value="" />
                                                <label for="id_ref_siranap_kode_ruang_perawatan">ID ref siranap kode ruang perawatan <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="id_ref_rsonline_fasyankes" name="id_ref_rsonline_fasyankes" placeholder="" value="" />
                                                <label for="id_ref_rsonline_fasyankes">ID ref rsonline fasyankes <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="prefix_antrean" name="prefix_antrean" placeholder="" value="" />
                                                <label for="prefix_antrean">Prefix Antrean <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="perawatan" name="perawatan" placeholder="" value="" />
                                                <label for="perawatan">Perawatan <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="kelas" name="kelas" placeholder="" value="" />
                                                <label for="kelas">Kelas <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="datepicker" class="form-control" id="kode_siranap_kelas_perawatan" name="kode_siranap_kelas_perawatan" placeholder="" value="" />
                                                <label for="kode_siranap_kelas_perawatan">Kode siranap kelas perawatan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="datepicker" class="form-control" id="kode_siranap_ruang_perawatan" name="kode_siranap_ruang_perawatan" placeholder="" value="" />
                                                <label for="kode_siranap_ruang_perawatan">Kode siranap ruang perawatan <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <select class="form-select form-select-md" data-control="select2" name="dinkes_jenis_bed" id="dinkes_jenis_bed">
                                                    <option></option>
                                                    <option value="TIDAK ADA">TIDAK ADA</option>
                                                    <option value="VIP">VIP</option>
                                                    <option value="kelas_1">KELAS 1</option>
                                                    <option value="kelas_2">KELAS 2</option>
                                                    <option value="kelas_3">TIDAK ADA</option>
                                                    <option value="hcu">HCU</option>
                                                    <option value="iccu">ICCU</option>
                                                    <option value="icu_negatif_ventilator">ICU NEGATIF VENTILATOR</option>
                                                    <option value="icu_negatif_tanpa_ventilator">ICU NEGATIF TANPA VENTILATOR</option>
                                                    <option value="icu_tanpa_negatif_ventilator">ICU TANPA NEGATIF VENTILATOR</option>
                                                    <option value="icu_tanpa_negatif_tanpa_ventilator">ICU TANPA NEGATIF TANPA VENTILATOR</option>
                                                    <option value="isolasi_negatif">ISOLASI NEGATIF</option>
                                                    <option value="isolasi_tanpa_negatif">ISOLASI TANPA NEGATIF</option>
                                                    <option value="nicu_covid">NICU COVID</option>
                                                    <option value="perina_covid">PERINA COVID</option>
                                                    <option value="picu_covid">PICU COVID</option>
                                                    <option value="ok_covid">OK COVID</option>
                                                    <option value="hd_covid">HD COVID</option>
                                                    <option value="icu_covid_negatif_ventilator">ICU COVID NEGATIF VENTILATOR</option>
                                                    <option value="icu_covid_negatif_tanpa_ventilator">ICU COVID NEGATIF TANPA VENTILATOR</option>
                                                    <option value="icu_covid_tanpa_negatif_ventilator">ICU COVID TANPA NEGATIF VENTILATOR</option>
                                                    <option value="icu_covid_tanpa_negatif_tanpa_ventilator">ICU COVID TANPA NEGATIF TANPA VENTILATOR</option>
                                                </select>
                                                <label for="dinkes_jenis_bed">Dinkes jenis bed<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                             <div class="form-floating">
                                                <input type="text" class="form-control" id="tersediapria" name="tersediapria" placeholder="" value="" />
                                                <label for="tersediapria">Tersedia Pria<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="tersediawanita" name="tersediawanita" placeholder="" value="" />
                                                <label for="tersediawanita">Tersedia Wanita<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                 <select class="form-select form-select-md" data-control="select2" name="is_del" id="is_del">
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                </select>   
                                                <label for="is_del">IS Del <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control datepicker" id="del_date" name="del_date" placeholder="" value="" />
                                                <label for="del_date">Delete Date<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="del_by" name="del_by" placeholder="" value="" />
                                                <label for="del_by">Delete by <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="id_user" name="id_user" placeholder="" value="" />
                                                <label for="id_user">ID User<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="is_bor" name="is_bor" placeholder="" value="" />
                                                <label for="is_bor">IS BOR<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                 <select class="form-select form-select-md" data-control="select2" name="is_covid" id="is_covid">
                                                    <option value="TRUE">TRUE</option>
                                                    <option value="TRUE">FALSE</option>
                                                </select>   
                                                <label for="is_covid">IS Covid<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                <div class="d-flex justify-content-between">
                                    <a href="<?= base_url($this->_class) ?>" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
                                        <span class="indicator-label">Add</span>

                                        <span class="indicator-progress">Please wait...
                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->
</div>
<!--end::Content wrapper-->