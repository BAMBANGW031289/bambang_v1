<!--begin::Content wrapper-->
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?= $title ?></h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center gap-2 gap-lg-3">

            </div>
            <!--end::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form w-100" novalidate="novalidate" id="edit_master_hr" action="#">
                                <input type="hidden" name="id" value="<?= $detail['id']; ?>">
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="no_rm" name="no_rm" placeholder="" value="<?= $detail['no_rm']; ?>" />
                                                <label for="no_rm">No RM <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="profesi" name="profesi" placeholder="" value="<?= $detail['profesi']; ?>" />
                                                <label for="profesi">Profesi <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="profesi_jabatan" name="profesi_jabatan" placeholder="" value="<?= $detail['profesi_jabatan']; ?>" />
                                                <label for="profesi_jabatan">Profesi Jabatan <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="role_prefix" name="role_prefix" placeholder="" value="<?= $detail['role_prefix'];  ?>" />
                                                <label for="role_prefix">Role Prefix <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="full_name" name="full_name" placeholder="" value="<?= $detail['full_name'];  ?>" />
                                                <label for="full_name">Full Name <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="username" name="username" placeholder="" value="<?= $detail['username'];  ?>" />
                                                <label for="username">Username <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pin" name="pin" placeholder="" value="<?= $detail['pin'];  ?>" />
                                                <label for="pin">Pin <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="prefix_antrean" name="prefix_antrean" placeholder="" value="<?= $detail['prefix_antrean'];  ?>" />
                                                <label for="prefix_antrean">Prefix Antrean <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="akses_navbar_level1" name="akses_navbar_level1" placeholder="" value="<?= $detail['akses_navbar_level1'];  ?>" />
                                                <label for="akses_navbar_level1">Akses Navbar Level 1 <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="bpjs_kode_dokter" name="bpjs_kode_dokter" placeholder="" value="<?= $detail['bpjs_kode_dokter'];  ?>" />
                                                <label for="bpjs_kode_dokter">BPJS Kode Dokter <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control datepicker backdate" id="pin_renewal" name="pin_renewal" placeholder="" value="<?= $detail['pin_renewal'];  ?>" />
                                                <label for="pin_renewal">Pin Renewal<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control datepicker" id="job_start" name="job_start" placeholder="" value="<?= $detail['job_start'];  ?>" />                                      
                                                <label for="job_start">Job Start <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="datepicker" class="form-control datepicker" id="job_end" name="job_end" placeholder="" value="<?= $detail['job_end'];  ?>" />
                                                <label for="job_end">Job End<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <select class="form-select form-select-md" data-control="select2" name="tipe_jadwal" id="tipe_jadwal">
                                                    <option></option>
                                                    <option value="SHIFT">SHIFT</option>
                                                    <option value="TETAP">TETAP</option>
                                                </select>
                                                <label for="tipe_jadwal">Tipe Jadwal<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="nomor_induk_karyawan" name="nomor_induk_karyawan" placeholder="" value="<?= $detail['nomor_induk_karyawan'];  ?>" />
                                                <label for="nomor_induk_karyawan">Nomor Induk Karyawan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="kode_karyawan_fingerprint" name="kode_karyawan_fingerprint" placeholder="" value="<?= $detail['kode_karyawan_fingerprint'];  ?>" />
                                                <label for="kode_karyawan_fingerprint">Kode Karyawan Fingerprint <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="npwp" name="npwp" placeholder="" value="<?= $detail['npwp'];  ?>" />
                                                <label for="npwp">NPWP<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="nama_bank" name="nama_bank" placeholder="" value="<?= $detail['nama_bank'];  ?>" />
                                                <label for="nama_bank">Nama Bank <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="no_rekening" name="no_rekening" placeholder="" value="<?= $detail['no_rekening'];  ?>" />
                                                <label for="no_rekening">No Rekening<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="no_ktp" name="no_ktp" placeholder="" value="<?= $detail['no_ktp'];  ?>" />
                                                <label for="no_ktp">No KTP <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="nama_satuan_kerja" name="nama_satuan_kerja" placeholder="" value="<?= $detail['nama_satuan_kerja'];  ?>" />
                                                <label for="nama_satuan_kerja">Nama Satuan Kerja<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="eselon" name="eselon" placeholder="" value="<?= $detail['eselon'];  ?>" />
                                                <label for="eselon">Eselon <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="golongan" name="golongan" placeholder="" value="<?= $detail['golongan'];  ?>" />
                                                <label for="golongan">Golongan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="nomor_induk_karyawan_pasangan" name="nomor_induk_karyawan_pasangan" placeholder="" value="<?= $detail['nomor_induk_karyawan_pasangan'];  ?>" />
                                                <label for="nomor_induk_karyawan_pasangan">Nomor Induk Karyawan Pasangan <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="" value="<?= $detail['no_telp'];  ?>" />
                                                <label for="no_telp">No Telp<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="jumlah_anak" name="jumlah_anak" placeholder="" value="<?= $detail['jumlah_anak'];  ?>" />
                                                <label for="jumlah_anak">Jumlah Anak <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="satusehat_id_practitioner" name="satusehat_id_practitioner" placeholder="" value="<?= $detail['satusehat_id_practitioner'];  ?>" />
                                                <label for="satusehat_id_practitioner">Satusehat ID Practitioner<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="status" name="status" placeholder="" value="<?= $detail['status'];  ?>" />
                                                <label for="status">Status<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                                <div class="form-floating">
                                                    <select class="form-select form-select-md" data-control="select2" name="akses" id="akses">
                                                        <option></option>
                                                        <option value="ON">ON</option>
                                                        <option value="OFF">OFF</option>
                                                    </select>
                                                <label for="akses">Akses<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="gaji_bulanan" name="gaji_bulanan" placeholder="" value="<?= $detail['gaji_bulanan'];  ?>" />
                                                <label for="gaji_bulanan">Gaji Bulanan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="str" name="str" placeholder="" value="<?= $detail['str'];  ?>" />
                                                <label for="str">STR<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control datepicker" id="str_exp_date" name="str_exp_date" placeholder="" value="<?= $detail['str_exp_date'];  ?>" />
                                                <label for="str_exp_date">STR Exp Date<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="sip" name="sip" placeholder="" value="<?= $detail['sip'];  ?>" />
                                                <label for="sip">SIP<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="datepicker" class="form-control datepicker" id="sip_exp_date" name="sip_exp_date" placeholder="" value="<?= $detail['sip_exp_date'];  ?>" />
                                                <label for="sip_exp_date">SIP Exp Date<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="ktk" name="ktk" placeholder="" value="<?= $detail['ktk'];  ?>" />
                                                <label for="ktk">KTK<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="group_unit_kerja" name="group_unit_kerja" placeholder="" value="<?= $detail['group_unit_kerja'];  ?>" />
                                                <label for="group_unit_kerja">Group Unit Kerja<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="unit_kerja" name="unit_kerja" placeholder="" value="<?= $detail['unit_kerja'];  ?>" />
                                                <label for="unit_kerja">Unit Kerja<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="jenis_karyawan" name="jenis_karyawan" placeholder="" value="<?= $detail['jenis_karyawan'];  ?>" />
                                                <label for="jenis_karyawan">Jenis Karyawan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="" value="<?= $detail['jabatan'];  ?>" />
                                                <label for="jabatan">Jabatan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pendidikan" name="pendidikan" placeholder="" value="<?= $detail['pendidikan'];  ?>" />
                                                <label for="pendidikan">Pendidikan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="nama_universitas" name="nama_universitas" placeholder="" value="<?= $detail['nama_universitas'];  ?>" />
                                                <label for="nama_universitas">Nama Universitas<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="status_karyawan" name="status_karyawan" placeholder="" value="<?= $detail['status_karyawan'];  ?>" />
                                                <label for="status_karyawan">Status Karyawan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="gaji_bruto" name="gaji_bruto" placeholder="" value="<?= $detail['gaji_bruto'];  ?>" />
                                                <label for="gaji_bruto">Gaji Bruto<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_pulsa" name="tunjangan_pulsa" placeholder="" value="<?= $detail['tunjangan_pulsa'];  ?>" />
                                                <label for="tunjangan_pulsa">Tunjangan Pulsa<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_bbm" name="tunjangan_bbm" placeholder="" value="<?= $detail['tunjangan_bbm'];  ?>" />
                                                <label for="tunjangan_bbm">Tunjangan BBM<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_sekretaris" name="tunjangan_sekretaris" placeholder="" value="<?= $detail['tunjangan_sekretaris'];  ?>" />
                                                <label for="tunjangan_sekretaris">Tunjangan Sekretaris<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_makeup" name="tunjangan_makeup" placeholder="" value="<?= $detail['tunjangan_makeup'];  ?>" />
                                                <label for="tunjangan_makeup">Tunjangan Makeup<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_internal_memo" name="tunjangan_internal_memo" placeholder="" value="<?= $detail['tunjangan_internal_memo'];  ?>" />
                                                <label for="tunjangan_internal_memo">Tunjangan Internal Memo<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_mengelola_data" name="tunjangan_mengelola_data" placeholder="" value="<?= $detail['tunjangan_mengelola_data'];  ?>" />
                                                <label for="tunjangan_mengelola_data">Tunjangan Mengelola Data<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_legal" name="tunjangan_legal" placeholder="" value="<?= $detail['tunjangan_legal'];  ?>" />
                                                <label for="tunjangan_legal">Tunjangan Legal<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_marketing" name="tunjangan_marketing" placeholder="" value="<?= $detail['tunjangan_marketing'];  ?>" />
                                                <label for="tunjangan_marketing">Tunjangan Marketing<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_resiko" name="tunjangan_resiko" placeholder="" value="<?= $detail['tunjangan_resiko'];  ?>" />
                                                <label for="tunjangan_resiko">Tunjangan Resiko<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_sip" name="tunjangan_sip" placeholder="" value="<?= $detail['tunjangan_sip'];  ?>" />
                                                <label for="tunjangan_sip">Tunjangan SIP<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_casemix" name="tunjangan_casemix" placeholder="" value="<?= $detail['tunjangan_casemix'];  ?>" />
                                                <label for="tunjangan_casemix">Tunjangan Casemix<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_makan" name="tunjangan_makan" placeholder="" value="<?= $detail['tunjangan_makan'];  ?>" />
                                                <label for="tunjangan_makan">Tunjangan Makanan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_transportasi" name="tunjangan_transportasi" placeholder="" value="<?= $detail['tunjangan_transportasi'];  ?>" />
                                                <label for="tunjangan_transportasi">Tunjangan Transportasi<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_kinerja" name="tunjangan_kinerja" placeholder="" value="<?= $detail['tunjangan_kinerja'];  ?>" />
                                                <label for="tunjangan_kinerja">Tunjangan Kinerja<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_lain_1" name="tunjangan_lain_1" placeholder="" value="<?= $detail['tunjangan_lain_1'];  ?>" />
                                                <label for="tunjangan_lain_1">Tunjangan Lain 1<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="tunjangan_lain_2" name="tunjangan_lain_2" placeholder="" value="<?= $detail['tunjangan_lain_2'];  ?>" />
                                                <label for="tunjangan_lain_2">Tunjangan Lain 2<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jpk_4" name="jpk_4" placeholder="" value="<?= $detail['jpk_4'];  ?>" />
                                                <label for="jpk_4">JPK 4<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jkk_jkn_054" name="jkk_jkn_054" placeholder="" value="<?= $detail['jkk_jkn_054'];  ?>" />
                                                <label for="jkk_jkn_054">JKK JKN 054<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jpk" name="jpk" placeholder="" value="<?= $detail['jpk'];  ?>" />
                                                <label for="jpk">JPK<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jkk" name="jkk" placeholder="" value="<?= $detail['jkk'];  ?>" />
                                                <label for="jkk">JKK<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jkm" name="jkm" placeholder="" value="<?= $detail['jkm'];  ?>" />
                                                <label for="jkm">JKM<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="gaji_total" name="gaji_total" placeholder="" value="<?= $detail['gaji_total'];  ?>" />
                                                <label for="gaji_total">Gaji Total<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="biaya_jabatan" name="biaya_jabatan" placeholder="" value="<?= $detail['biaya_jabatan'];  ?>" />
                                                <label for="biaya_jabatan">Biaya Jabatan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="iuran_jht" name="iuran_jht" placeholder="" value="<?= $detail['iuran_jht'];  ?>" />
                                                <label for="iuran_jht">Iuran JHT<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="iuran_jht_2" name="iuran_jht_2" placeholder="" value="<?= $detail['iuran_jht_2'];  ?>" />
                                                <label for="iuran_jht_2">Iuran JHT 2<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="pengurangan_total" name="pengurangan_total" placeholder="" value="<?= $detail['pengurangan_total'];  ?>" />
                                                <label for="pengurangan_total">Pengurangan Total<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="ph_netto_bulan" name="ph_netto_bulan" placeholder="" value="<?= $detail['ph_netto_bulan'];  ?>" />
                                                <label for="ph_netto_bulan">PH Netto Bulan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="ph_netto_tahun" name="ph_netto_tahun" placeholder="" value="<?= $detail['ph_netto_tahun'];  ?>" />
                                                <label for="ph_netto_tahun">PH Netto Tahun<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="ptkp" name="ptkp" placeholder="" value="<?= $detail['ptkp'];  ?>" />
                                                <label for="ptkp">PTKP<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pkp" name="pkp" placeholder="" value="<?= $detail['pkp'];  ?>" />
                                                <label for="pkp">PKP<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pajak_5" name="pajak_5" placeholder="" value="<?= $detail['pajak_5'];  ?>" />
                                                <label for="pajak_5">Pajak 5<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pajak_15" name="pajak_15" placeholder="" value="<?= $detail['pajak_15'];  ?>" />
                                                <label for="pajak_15">Pajak 15<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pajak_25" name="pajak_25" placeholder="" value="<?= $detail['pajak_25'];  ?>" />
                                                <label for="pajak_25">Pajak 25<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pajak_30" name="pajak_30" placeholder="" value="<?= $detail['pajak_30'];  ?>" />
                                                <label for="pajak_30">Pajak 30<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pajak_35" name="pajak_35" placeholder="" value="<?= $detail['pajak_35'];  ?>" />
                                                <label for="pajak_35">Pajak 35<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pph_21_bulan" name="pph_21_bulan" placeholder="" value="<?= $detail['pph_21_bulan'];  ?>" />
                                                <label for="pph_21_bulan">pph 21 bulan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pph_21_tahun" name="pph_21_tahun" placeholder="" value="<?= $detail['pph_21_tahun'];  ?>" />
                                                <label for="pph_21_tahun">pph 21 tahun<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="pph" name="pph" placeholder="" value="<?= $detail['pph'];  ?>" />
                                                <label for="pph">pph<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="gaji_bersih" name="gaji_bersih" placeholder="" value="<?= $detail['gaji_bersih'];  ?>" />
                                                <label for="gaji_bersih">gaji_bersih<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="bon_sementara" name="bon_sementara" placeholder="" value="<?= $detail['bon_sementara'];  ?>" />
                                                <label for="bon_sementara">Bon Sementara<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="seragam" name="seragam" placeholder="" value="<?= $detail['seragam'];  ?>" />
                                                <label for="seragam">Seragam<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jaminan_hari_tua" name="jaminan_hari_tua" placeholder="" value="<?= $detail['jaminan_hari_tua'];  ?>" />
                                                <label for="jaminan_hari_tua">Jaminan Hari Tua<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jaminan_hari_tua_2" name="jaminan_hari_tua_2" placeholder="" value="<?= $detail['jaminan_hari_tua_2'];  ?>" />
                                                <label for="jaminan_hari_tua_2">Jaminan Hari Tua 2<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="rawat_inap" name="rawat_inap" placeholder="" value="<?= $detail['rawat_inap'];  ?>" />
                                                <label for="rawat_inap">Rawat Inap<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="apotek" name="apotek" placeholder="" value="<?= $detail['apotek'];  ?>" />
                                                <label for="apotek">Apotek<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jaminan_pensiun" name="jaminan_pensiun" placeholder="" value="<?= $detail['jaminan_pensiun'];  ?>" />
                                                <label for="jaminan_pensiun">Jaminan Pensiun<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jaminan_pensiun_2" name="jaminan_pensiun_2" placeholder="" value="<?= $detail['jaminan_pensiun_2'];  ?>" />
                                                <label for="jaminan_pensiun_2">Jaminan Pensiun 2<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="koperasi" name="koperasi" placeholder="" value="<?= $detail['koperasi'];  ?>" />
                                                <label for="koperasi">Koperasi<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="ikm" name="ikm" placeholder="" value="<?= $detail['ikm'];  ?>" />
                                                <label for="ikm">IKM<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="brk" name="brk" placeholder="" value="<?= $detail['brk'];  ?>" />
                                                <label for="brk">BRK<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="bpjs_kesehatan" name="bpjs_kesehatan" placeholder="" value="<?= $detail['bpjs_kesehatan'];  ?>" />
                                                <label for="bpjs_kesehatan">BPJS Kesehatan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="biaya_adm_bank" name="biaya_adm_bank" placeholder="" value="<?= $detail['biaya_adm_bank'];  ?>" />
                                                <label for="biaya_adm_bank">Biaya ADM Bank<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="gaji_netto" name="gaji_netto" placeholder="" value="<?= $detail['gaji_netto'];  ?>" />
                                                <label for="gaji_netto">Gaji Netto<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="prestasi" name="prestasi" placeholder="" value="<?= $detail['prestasi'];  ?>" />
                                                <label for="prestasi">Prestasi<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control rupiah" id="jasa_medis" name="jasa_medis" placeholder="" value="<?= $detail['jasa_medis'];  ?>" />
                                                <label for="jasa_medis">Jasa Medis<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="contract_details" name="contract_details" placeholder="" value="<?= $detail['contract_details'];  ?>" />
                                                <label for="contract_details">Contract Details<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="kontrak_isi" name="kontrak_isi" placeholder="" value="<?= $detail['kontrak_isi'];  ?>" />
                                                <label for="kontrak_isi">Kontrak Isi<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="kontrak_ttd_karyawan" name="kontrak_ttd_karyawan" placeholder="" value="<?= $detail['kontrak_ttd_karyawan'];  ?>" />
                                                <label for="kontrak_ttd_karyawan">Kontrak Ttd Karyawan<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="kontrak_ttd_hrd" name="kontrak_ttd_hrd" placeholder="" value="<?= $detail['kontrak_ttd_hrd'];  ?>" />
                                                <label for="kontrak_ttd_hrd">Kontrak Ttd HRD<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="kontrak_nama_hrd" name="kontrak_nama_hrd" placeholder="" value="<?= $detail['kontrak_nama_hrd'];  ?>" />
                                                <label for="kontrak_nama_hrd">Kontrak Nama HRD<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="datepicker" class="form-control" id="kontrak_datetime_karyawan" name="kontrak_datetime_karyawan" placeholder="" value="<?= $detail['kontrak_datetime_karyawan'];  ?>" />
                                                <label for="kontrak_datetime_karyawan">Kontrak Datetime Karyawan<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control datepicker" id="kontrak_datetime_hrd" name="kontrak_datetime_hrd" placeholder="" value="<?= $detail['kontrak_datetime_hrd'];  ?>" />
                                                <label for="kontrak_datetime_hrd">Kontrak Datetime HRD<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="datepicker" class="form-control" id="no_sk" name="no_sk" placeholder="" value="<?= $detail['no_sk'];  ?>" />
                                                <label for="no_sk">NO SK<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="datepicker" class="form-control" id="tanggal_sk" name="tanggal_sk" placeholder="" value="<?= $detail['tanggal_sk'];  ?>" />
                                                <label for="tanggal_sk">Tanggal SK<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="datepicker" class="form-control datepicker" id="tanggal_mutasi" name="tanggal_mutasi" placeholder="" value="<?= $detail['tanggal_mutasi'];  ?>" />
                                                <label for="tanggal_mutasi">Tanggal Mutasi<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="prefix" name="prefix" placeholder="" value="<?= $detail['prefix'];  ?>" />
                                                <label for="prefix">Prefix<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="alasan_resign" name="alasan_resign" placeholder="" value="<?= $detail['alasan_resign'];  ?>" />
                                                <label for="alasan_resign">Alasan Resign<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                            <select class="form-select form-select-md" data-control="select2" name="status_berhenti_kerja" id="status_berhenti_kerja">
                                                    <option value=''></option>
                                                    <option value="RESIGN">RESIGN</option>
                                                    <option value="FIRED">FIRED</option>
                                                </select>
                                                <label for="status_berhenti_kerja">Status Berhenti Kerja<anspan class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="unit" name="unit" placeholder="" value="<?= $detail['unit'];  ?>" />
                                                <label for="unit">Unit<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="persen_jasmed_tarif_khusus" name="persen_jasmed_tarif_khusus" placeholder="" value="<?= $detail['persen_jasmed_tarif_khusus'];  ?>" />
                                                <label for="persen_jasmed_tarif_khusus">Persen Jasmed Tarif Khusus<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="spesialisasi" name="spesialisasi" placeholder="" value="<?= $detail['spesialisasi'];  ?>" />
                                                <label for="spesialisasi">Spesialisasi<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <a href="<?= base_url($this->_class) ?>" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
                                        <span class="indicator-label">Add</span>

                                        <span class="indicator-progress">Please wait...
                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->
</div>
<!--end::Content wrapper-->