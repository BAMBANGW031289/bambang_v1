<!--begin::Content wrapper-->
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?= $title ?></h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center gap-2 gap-lg-3">

            </div>
            <!--end::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form w-100" novalidate="novalidate" id="edit_master_log" action="#">
                                <input type="hidden" name="id" value="<?= $detail['id']; ?>">
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                    <select class="form-select form-select-md" data-control="select2" name="tipe_email_sent" id="tipe_email_sent">
                                                        <option></option>
                                                        <option value="MCU">MCU</option>
                                                        <option value="RAD">RAD</option>
                                                        <option value="LAB">LAB</option>
                                                        <option value="PO">PO</option>
                                                        <option value="PRODUK_EXPIRED">PRODUK_EXPIRED</option>
                                                        <option value="HRD">HRD</option>
                                                    </select>
                                                <label for="tipe_email_sent">tipe_email_sent <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="no_ref" name="no_ref" placeholder="" value="<?= $detail['no_ref']; ?>" />
                                                <label for="no_ref">No Ref <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="email_sent_to" name="email_sent_to" placeholder="" value="<?= $detail['email_sent_to']; ?>" />
                                                <label for="email_sent_to">Email sent to <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <a href="<?= base_url($this->_class) ?>" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
                                        <span class="indicator-label">Add</span>

                                        <span class="indicator-progress">Please wait...
                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->
</div>
<!--end::Content wrapper-->