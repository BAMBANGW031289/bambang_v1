<!DOCTYPE html>
<html lang="en">

<head>
	<base href="../" />
	<title><?= !empty($title) ? $title : 'LokalPro'; ?></title>
	<meta charset="utf-8" />
	<meta name="description" content="LokalPro" />
	<meta name="keywords" content="LokalPro" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="LokalPro" />
	<meta property="og:site_name" content="Lokalpro" />
	<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
	<link rel="shortcut icon" href="<?= base_url('assets/template/metronic') ?>/media/logos/favicon.ico" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
	<link href="<?= base_url('assets/template/metronic') ?>/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/template/metronic') ?>/plugins/custom/sweetalert2/css/sweetalert2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/template/metronic') ?>/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/template/metronic') ?>/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>

<body id="kt_app_body" data-kt-app-layout="light-header" data-kt-app-header-fixed="true" data-kt-app-toolbar-enabled="true" class="app-default">
	<script>
		var defaultThemeMode = "light";
		var themeMode;
		if (document.documentElement) {
			if (document.documentElement.hasAttribute("data-bs-theme-mode")) {
				themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
			} else {
				if (localStorage.getItem("data-bs-theme") !== null) {
					themeMode = localStorage.getItem("data-bs-theme");
				} else {
					themeMode = defaultThemeMode;
				}
			}
			if (themeMode === "system") {
				themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
			}
			document.documentElement.setAttribute("data-bs-theme", themeMode);
		}
	</script>
	<div class="d-flex flex-column flex-root app-root" id="kt_app_root">
		<div class="app-page flex-column flex-column-fluid" id="kt_app_page">
			<?php $this->load->view('qwerty_v1/layout/header/header'); ?>
			<div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
				<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
					<?php $this->load->view($contents); ?>
					<?php $this->load->view('qwerty_v1/layout/footer/footer'); ?>
				</div>
			</div>
		</div>
	</div>
	<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
		<i class="ki-duotone ki-arrow-up">
			<span class="path1"></span>
			<span class="path2"></span>
		</i>
	</div>

	<script>
		const target = "<?= base_url($this->uri->segments[1]); ?>";
		const page_url = "<?= base_url($this->class); ?>";
		const base_url = "<?= base_url(); ?>";
	</script>
	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script src="<?= base_url('assets/template/metronic'); ?>/plugins/global/plugins.bundle.js"></script>
	<script src="<?= base_url('assets/template/metronic'); ?>/js/scripts.bundle.js"></script>
	<script src="<?= base_url('assets/template/metronic'); ?>/plugins/custom/datatables/datatables.bundle.js"></script>
	<script src="<?= base_url('assets/template/metronic'); ?>/plugins/custom/loadingoverlay/js/loading-overlay.js"></script>
	<script src="<?= base_url('assets/template/metronic'); ?>/plugins/custom/sweetalert2/js/sweetalert2.all.min.js"></script>
	<script src="<?= base_url('assets/template/metronic'); ?>/js/mvc/custom.js"></script>
	<?php if (isset($js)) : ?>
		<?php foreach ($js as $k => $v) : ?>
			<script src="<?= $v; ?>"></script>
		<?php endforeach; ?>
	<?php endif; ?>
	<script>
	// format datepicker	
	$(".datepicker-time").flatpickr({
		enableTime: true,
		dateFormat: "Y-m-d H:i", 
	})
	$(".datepicker").flatpickr()

	// membatasi back date
	$(function(){
		var dtToday = new Date();

		var month = dtToday.getMonth() + 1;
		var day = dtToday.getDate();
		var year = dtToday.getFullYear();
		if(month < 10)
			month = '0' + month.toString();
		if(day < 10)
			day = '0' + day.toString();

		var minDate= year + '-' + month + '-' + day;

		$('.backdate').attr('min', minDate);
    });

	// format rupiah
	function formatRupiah(nominal){
			return new Intl.NumberFormat('id-ID',
					{ style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
				).format(nominal);
			}
			$('.rupiah').keyup(function(){
			var numericValue = parseFloat(this.value.replace(/[^\d]/g, ''));
			if (!isNaN(numericValue)) {
				var formattedValue = formatRupiah(numericValue);
				$(this).val(formattedValue);
			}
			else{
				$(this).val('');
			}
	   });
</script>	


</body>

</html>