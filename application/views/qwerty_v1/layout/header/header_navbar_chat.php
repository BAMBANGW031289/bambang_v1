<div class="app-navbar-item ms-1 ms-md-3">
  <!--begin::Menu wrapper-->
  <div
    class="btn btn-icon btn-custom btn-icon-muted btn-active-light btn-active-color-primary w-30px h-30px w-md-40px h-md-40px position-relative"
    id="kt_drawer_chat_toggle">
    <i class="ki-duotone ki-message-text-2 fs-2 fs-lg-1">
      <span class="path1"></span>
      <span class="path2"></span>
      <span class="path3"></span>
    </i>
    <span
      class="bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-50 animation-blink"></span>
  </div>
  <!--end::Menu wrapper-->
</div>