<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
  public function __construct(){
		parent::__construct();
		$this->load->model('qwerty_v1/master_v1/M_user', 'model_user');
	  }

	public function index(){
		if($this->session->userdata('authenticated')) // Jika user sudah login (Session authenticated ditemukan)
		  redirect('master_hr'); // Redirect ke page home
		  $this->load->view('welcome_message');
	  }

	public function login(){
		$username = $this->input->post('username'); // Ambil isi dari inputan username pada form login
		$password = md5($this->input->post('password')); // Ambil isi dari inputan password pada form login dan encrypt dengan md5
		$user = $this->model_user->get($username); // Panggil fungsi get yang ada di model_user.php
		if(empty($user)){ // Jika hasilnya kosong / user tidak ditemukan
		  $this->session->set_flashdata('message', 'Username tidak ditemukan'); // Buat session flashdata
		  redirect('login'); // Redirect ke halaman login
		}else{
		  if($password == $user->password){ // Jika password yang diinput sama dengan password yang didatabase
			$session = array(
			  'authenticated'=>true, // Buat session authenticated dengan value true
			  'username'=>$user->username,  // Buat session username
			  'nama'=>$user->nama, // Buat session nama
			  'role'=>$user->role // Buat session role
			);
			$this->session->set_userdata($session); // Buat session sesuai $session
			redirect('master_hr'); // Redirect ke halaman home
		  }else{
			$this->session->set_flashdata('message', 'Password salah'); // Buat session flashdata
			redirect('login'); // Redirect ke halaman login
		  }
		}
	  }

	  public function logout(){
		$this->session->sess_destroy(); // Hapus semua session
		redirect('login'); // Redirect ke halaman login
	  }
}

