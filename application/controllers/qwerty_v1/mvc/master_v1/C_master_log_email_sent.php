<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_master_log_email_sent extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->_class   = $this->uri->segments[1];
    $this->_view    = 'qwerty_v1/contents/master_v1/master_log_email_sent';
    $this->load->model('qwerty_v1/master_v1/M_master_log_email_sent', 'master_log_email_sent');
    $this->load->library('form_validation');
    $this->id = 1;
  }


  public function index()
  {
    $data['title']    = "Master Log Email Sent";
    $data['url']      = base_url($this->_class);
    $data['contents'] = $this->_view . '/index';
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_log_email_sent/index.js',
    ];
    $data['css'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_log_email_sent/style.css',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  function json()
  {
    $value = $this->_postvalue()['data'];
    $json = [
      'draw' => $this->input->post('draw'),
      'recordsTotal' => $value['record_total'],
      'recordsFiltered' => $value['record_filter']
    ];

    $data = [];

    $i = $_POST['start'];
    foreach ($value['result'] as $k => $v) {
      $i++;
      $row = [];
      $button = '<a href="' . base_url($this->_class . '/edit?id=' . $v['id']) . '" class="btn btn-sm btn-primary">Edit</a>';
      $button .= '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $v['id'] . '" style="margin-left:5px">Hapus</button>';
      $row[] = $i;
      $row[] = $button;
      $row[] = $v['tipe_email_sent'];
      $row[] = $v['no_ref'];
      $row[] = $v['email_sent_to'];

      $data[] = $row;
    }

    $json['data'] = $data;

    echo json_encode($json);
  }

  function _postvalue($paging = TRUE)
  {
    $params = [
      'offset' => $this->input->post('start'),
      'limit' => $this->input->post('length'),
      'search' => $this->input->post('search')['value'],
    ];


    if ($_POST['search']['value']) {
      $params['search'] = $_POST['search']['value'] ? $_POST['search']['value'] : '';
    }

    if (isset($_POST['ORDER'])) {
      if ($_POST['order']['0']['column']) {
        $params['order_column'] = $_POST['order']['0']['column'] ? $_POST['order']['0']['column'] : '2';
      }
      if ($_POST['order']['0']['dir']) {
        $params['order_dir'] = $_POST['order']['0']['dir'] ? $_POST['order']['0']['dir'] : 'asc';
      }
    }

    if (!$paging) {
      unset($params['offset']);
      unset($params['limit']);
    }

    $value = $this->master_log_email_sent->get($params);
    return $value;
  }

  public function add($params = null)
  {
    $data['title']    = "Add Master Log email Sent";
    $data['contents'] = $this->_view . '/add';
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_log_email_sent/add.js',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  public function add_process($params = null)
  {
    $this->form_validation->set_rules('tipe_email_sent', 'Email Sent To', 'required');
	  $this->form_validation->set_rules('no_ref', 'No Ref', 'required');	
	  $this->form_validation->set_rules('email_sent_to', 'Email Sent To', 'required');	

    if($this->form_validation->run() == TRUE){
    $post = $this->input->post();
    $res_add = $this->master_log_email_sent->add([
      'tipe_email_sent'       => $post['tipe_email_sent'],
      'no_ref'                => $post['no_ref'],
      'email_sent_to'         => $post['email_sent_to'],
      'created_date'          => date("Y-m-d h:i:sa"),
      'created_by'            => "admin",
    ]);
         echo json_encode($res_add);
		}else{
			echo json_encode(
				array(
					'success' => false,
					'message' => 'Data Gagal Disimpan!'
				)
			);
		}
  }

  public function edit($params = null)
  {
    $data['title']    = "Edit Master Pendidikan";
    $data['contents'] = $this->_view . '/edit';
    $data['detail'] = $this->master_log_email_sent->get(['id' => $this->input->get('id')])['data']['result'][0];
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_log_email_sent/edit.js',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  public function edit_process($id = null)
  {
    $post = $this->input->post();
    $data = [
      'id'                    => $post['id'],
      'tipe_email_sent'       => $post['tipe_email_sent'],
      'no_ref'                => $post['no_ref'],
      'email_sent_to'         => $post['email_sent_to'],
      'created_date'          => date("Y-m-d h:i:sa"),
      'created_by'            => "admin",
    ];
    $res_update  = $this->master_log_email_sent->update($data['id'], $data);
    echo json_encode($res_update);
  }

  public function delete($params = null)
  {
    $post = $this->input->post();
    $res_delete = $this->master_log_email_sent->delete([
      'id'  => $post['id'],
    ]);
    echo json_encode($res_delete);
  }
}

/* End of file C_master_log_email_sent.php */
