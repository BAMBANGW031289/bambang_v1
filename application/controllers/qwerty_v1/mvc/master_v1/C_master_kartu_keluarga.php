<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_master_kartu_keluarga extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->_class   = $this->uri->segments[1];
    $this->_view    = 'qwerty_v1/contents/master_v1/master_kartu_keluarga';
    $this->load->model('qwerty_v1/master_v1/M_master_kartu_keluarga', 'master_kartu_keluarga');
    $this->load->library('form_validation');
    $this->id = 1;			
  }


  public function index()
  {
    $data['title']    = "Master Kartu Keluarga";
    $data['url']      = base_url($this->_class);
    $data['contents'] = $this->_view . '/index';
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_kartu_keluarga/index.js',
    ];
    $data['css'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_kartu_keluarga/style.css',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  function json()
  {
    $value = $this->_postvalue()['data'];
    $json = [
      'draw' => $this->input->post('draw'),
      'recordsTotal' => $value['record_total'],
      'recordsFiltered' => $value['record_filter']
    ];

    $data = [];

    $i = $_POST['start'];
    foreach ($value['result'] as $k => $v) {
      $i++;
      $row = [];
      $button = '<a href="' . base_url($this->_class . '/edit?id=' . $v['id']) . '" class="btn btn-sm btn-primary">Edit</a>';
      $button .= '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $v['id'] . '" style="margin-left:5px">Hapus</button>';
      $row[] = $i;
      $row[] = $button;
      $row[] = $v['nomor_kartu_keluarga'];
      $row[] = $v['alamat_rumah'];
      $row[] = $v['kode_pos'];

      $data[] = $row;
    }

    $json['data'] = $data;

    echo json_encode($json);
  }

  function _postvalue($paging = TRUE)
  {
    $params = [
      'offset' => $this->input->post('start'),
      'limit' => $this->input->post('length'),
      'search' => $this->input->post('search')['value'],
    ];


    if ($_POST['search']['value']) {
      $params['search'] = $_POST['search']['value'] ? $_POST['search']['value'] : '';
    }

    if (isset($_POST['ORDER'])) {
      if ($_POST['order']['0']['column']) {
        $params['order_column'] = $_POST['order']['0']['column'] ? $_POST['order']['0']['column'] : '2';
      }
      if ($_POST['order']['0']['dir']) {
        $params['order_dir'] = $_POST['order']['0']['dir'] ? $_POST['order']['0']['dir'] : 'asc';
      }
    }

    if (!$paging) {
      unset($params['offset']);
      unset($params['limit']);
    }

    $value = $this->master_kartu_keluarga->get($params);
    return $value;
  }

  public function add($params = null)
  {
    $data['title']    = "Add Master Kartu Keluarga";
    $data['contents'] = $this->_view . '/add';
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_kartu_keluarga/add.js',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  public function add_process($params = null)
  {
    $post = $this->input->post();
    $res_add = $this->master_kartu_keluarga->add([
      'id_users_profile'       => $post['id_users_profile'],
      'nomor_kartu_keluarga'   => $post['nomor_kartu_keluarga'],
      'alamat_rumah'           => $post['alamat_rumah'],
      'kode_pos'               => $post['kode_pos'],
      'created_date'           => $post['created_date'],
      'jakarta_bandung'        => $post['jakarta_bandung'],
      'simpat_proxl'           => $post['simpat_proxl'],
    ]);
    echo json_encode($res_add);
  }

  public function edit($params = null)
  {
    $data['title']    = "Edit Master Kartu Keluarga";
    $data['contents'] = $this->_view . '/edit';
    $data['detail'] = $this->master_kartu_keluarga->get(['id' => $this->input->get('id')])['data']['result'][0];
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_kartu_keluarga/edit.js',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  public function edit_process($params = null)
  {
    $post = $this->input->post();
    $res_update = $this->master_kartu_keluarga->update([
      'id'                     => $post['id'],
      'id_users_profile'       => $post['id_users_profile'],
      'nomor_kartu_keluarga'   => $post['nomor_kartu_keluarga'],
      'alamat_rumah'           => $post['alamat_rumah'],
      'kode_pos'               => $post['kode_pos'],
      'created_date'           => $post['created_date'],
      'jakarta_bandung'        => $post['jakarta_bandung'],
      'simpat_proxl'           => $post['simpat_proxl'],
    ]);
    echo json_encode($res_update);
  }

  public function delete($params = null)
  {
    $post = $this->input->post();
    $res_delete = $this->master_kartu_keluarga->delete([
      'id'  => $post['id'],
    ]);
    echo json_encode($res_delete);
  }
}

/* End of file C_master_kartu_keluarga.php */
