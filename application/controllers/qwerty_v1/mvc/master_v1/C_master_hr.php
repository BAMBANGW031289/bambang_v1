<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_master_hr extends MY_Controller 
{
	public function __construct()
	{
			parent::__construct();
			$this->_class   = $this->uri->segments[1];
			$this->_view    = 'qwerty_v1/contents/master_v1/master_hr';
			$this->load->model('qwerty_v1/master_v1/M_master_hr', 'master_hr');
			$this->load->library('form_validation');
			$this->id = 1;				
	}

	public function index()
	{
	  $data['title']    = "Master HR";
	  $data['url']      = base_url($this->_class);
	  $data['contents'] = $this->_view . '/index';
	  $data['js'] = [
		$this->_assets . 'qwerty_v1/js/master_v1/master_hr/index.js',
	  ];
	  $data['css'] = [
		$this->_assets . 'qwerty_v1/js/master_v1/master_hr/style.css',
	  ];
  
	  $this->load->view('qwerty_v1/layout/master', $data);
	}

	function json()
    {
		$value = $this->_postvalue()['data'];
		$json = [
		'draw' => $this->input->post('draw'),
		'recordsTotal' => $value['record_total'],
		'recordsFiltered' => $value['record_filter']
		];

		$data = [];

		$i = $_POST['start'];
		foreach ($value['result'] as $k => $v) {
		$i++;
		$row = [];
		$button = '<a href="' . base_url($this->_class . '/edit?id=' . $v['id']) . '" class="btn btn-sm btn-primary">Edit</a>';
		$button .= '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $v['id'] . '" style="margin-left:5px">Hapus</button>';
		$row[] = $i;
		$row[] = $button;
		$row[] = $v['no_rm'];
		$row[] = $v['profesi'];
		$row[] = $v['full_name'];

		$data[] = $row;
		}

		$json['data'] = $data;
		echo json_encode($json);
    }

	function _postvalue($paging = TRUE)
	{
	  $params = [
		'offset' => $this->input->post('start'),
		'limit' => $this->input->post('length'),
		'search' => $this->input->post('search')['value'],
	  ];
  
  
	  if ($_POST['search']['value']) {
		$params['search'] = $_POST['search']['value'] ? $_POST['search']['value'] : '';
	  }
  
	  if (isset($_POST['ORDER'])) {
		if ($_POST['order']['0']['column']) {
		  $params['order_column'] = $_POST['order']['0']['column'] ? $_POST['order']['0']['column'] : '2';
		}
		if ($_POST['order']['0']['dir']) {
		  $params['order_dir'] = $_POST['order']['0']['dir'] ? $_POST['order']['0']['dir'] : 'asc';
		}
	  }
  
	  if (!$paging) {
		unset($params['offset']);
		unset($params['limit']);
	  }
  
	  $value = $this->master_hr->get($params);
	  return $value;
	}
  
	public function add($params = null)
	{
	  $data['title']    = "Add Master HR";
	  $data['contents'] = $this->_view . '/add';
	  $data['js'] = [
		$this->_assets . 'qwerty_v1/js/master_v1/master_hr/add.js',
	  ];
  
	  $this->load->view('qwerty_v1/layout/master', $data);
	}
  
	public function add_process($params = null)
	{
	  $this->form_validation->set_rules('no_rm', 'No RM', 'required');
	  $this->form_validation->set_rules('profesi', 'Profesi', 'required');	
	  $this->form_validation->set_rules('nomor_induk_karyawan', 'Nomor Induk Karyawan', 'required');	
	  $this->form_validation->set_rules('bpjs_kode_dokter', 'BPJS Kode Dokter', 'required');
	  $this->form_validation->set_rules('full_name', 'Full Name', 'required');	
	  $this->form_validation->set_rules('username', 'Username', 'required');	
	  $this->form_validation->set_rules('kode_karyawan_fingerprint', 'Kode Karyawan Fingerprint', 'required');


	  if($this->form_validation->run() == TRUE){	
	  
	  $post = $this->input->post();
	  $res_add = $this->master_hr->add([
	    'no_rm' 			   		     => $post['no_rm'],		
		'profesi' 			   			 => $post['profesi'],	
		'profesi_jabatan'      			 => $post['profesi_jabatan'],	
		'role_prefix'          			 => $post['role_prefix'],	
		'full_name'           			 => $post['full_name'],	
		'username'             			 => $post['username'],	
		'pin'                  			 => $post['pin'],	
		'prefix_antrean'       			 => $post['prefix_antrean'],	
		'akses_navbar_level1'  			 => $post['akses_navbar_level1'],	
		'bpjs_kode_dokter'     			 => $post['bpjs_kode_dokter'],	
		'pin_renewal'          			 => $post['pin_renewal'],
		'job_start'            			 => $post['job_start'],
		'job_end'              			 => $post['job_end'],
		'tipe_jadwal'          			 => $post['tipe_jadwal'],
		'nomor_induk_karyawan' 			 => $post['nomor_induk_karyawan'],
		'kode_karyawan_fingerprint' 	 => $post['kode_karyawan_fingerprint'],
		'npwp'                      	 => $post['npwp'],
		'nama_bank'                 	 => $post['nama_bank'],
		'no_rekening'               	 => $post['no_rekening'],
		'no_ktp'                    	 => $post['no_ktp'],
		'nama_satuan_kerja'         	 => $post['nama_satuan_kerja'],
	    'eselon'                         => $post['eselon'],
		'golongan'                 		 => $post['golongan'],
		'nomor_induk_karyawan_pasangan'  => $post['nomor_induk_karyawan_pasangan'],
		'no_telp'                        => $post['no_telp'],
		'jumlah_anak'                    => $post['jumlah_anak'],
		'satusehat_id_practitioner'      => $post['satusehat_id_practitioner'],
		'status'                         => $post['status'],
		'akses'                          => $post['akses'],
		'gaji_bulanan'                   => $post['gaji_bulanan'],
		'str'                            => $post['str'],
		'str_exp_date'                   => $post['str_exp_date'],
		'sip'                            => $post['sip'],
		'sip_exp_date'                   => $post['sip_exp_date'],
		'ktk'                            => $post['ktk'],
		'group_unit_kerja'               => $post['group_unit_kerja'],
		'unit_kerja'                     => $post['unit_kerja'],
		'jenis_karyawan'                 => $post['jenis_karyawan'],
		'jabatan'                        => $post['jabatan'],
		'pendidikan'                     => $post['pendidikan'],
		'nama_universitas'               => $post['nama_universitas'],
		'status_karyawan'                => $post['status_karyawan'],
		'gaji_bruto'                     => $post['gaji_bruto'],
		'tunjangan_pulsa'                => $post['tunjangan_pulsa'], 
		'tunjangan_bbm'                  => $post['tunjangan_bbm'],
		'tunjangan_sekretaris'           => $post['tunjangan_sekretaris'],
		'tunjangan_makeup'               => $post['tunjangan_makeup'],
		'tunjangan_internal_memo'        => $post['tunjangan_internal_memo'],
		'tunjangan_mengelola_data'       => $post['tunjangan_mengelola_data'],
		'tunjangan_legal'                => $post['tunjangan_legal'],
		'tunjangan_marketing'            => $post['tunjangan_marketing'],
		'tunjangan_resiko'               => $post['tunjangan_resiko'],
		'tunjangan_sip'                  => $post['tunjangan_sip'],
		'tunjangan_casemix'              => $post['tunjangan_casemix'],
		'tunjangan_makan'                => $post['tunjangan_makan'],
		'tunjangan_transportasi'         => $post['tunjangan_transportasi'],
		'tunjangan_kinerja'              => $post['tunjangan_kinerja'],
		'tunjangan_lain_1'               => $post['tunjangan_lain_1'],
		'tunjangan_lain_2'               => $post['tunjangan_lain_2'],
		'jpk_4'                          => $post['jpk_4'],
		'jkk_jkn_054'                    => $post['jkk_jkn_054'],
		'jpk'                            => $post['jpk'],
		'jkk'                            => $post['jkk'],
		'jkm'                            => $post['jkm'],
		'gaji_total'                     => $post['gaji_total'],
		'biaya_jabatan'                  => $post['biaya_jabatan'],
		'iuran_jht'                      => $post['iuran_jht'],
		'iuran_jht_2'                    => $post['iuran_jht_2'],
		'pengurangan_total'              => $post['pengurangan_total'],
		'ph_netto_bulan'                 => $post['ph_netto_bulan'],
		'ph_netto_tahun'                 => $post['ph_netto_tahun'],   
		'ptkp'                           => $post['ptkp'],
		'pkp'                            => $post['pkp'],
		'pajak_5'                        => $post['pajak_5'],
		'pajak_15'                       => $post['pajak_15'],
		'pajak_25'                       => $post['pajak_25'],
		'pajak_30'                       => $post['pajak_30'],
		'pajak_35'                       => $post['pajak_35'],
		'pph_21_bulan'                   => $post['pph_21_bulan'],
		'pph_21_tahun'                   => $post['pph_21_tahun'],
		'pph'                            => $post['pph'],
		'gaji_bersih'                    => $post['gaji_bersih'],
		'bon_sementara'                  => $post['bon_sementara'],
		'seragam'                        => $post['seragam'],
		'jaminan_hari_tua'               => $post['jaminan_hari_tua'],
		'jaminan_hari_tua_2'             => $post['jaminan_hari_tua_2'],
		'rawat_inap'                     => $post['rawat_inap'],
		'apotek'                         => $post['apotek'],
		'jaminan_pensiun'                => $post['jaminan_pensiun'],
		'jaminan_pensiun_2'              => $post['jaminan_pensiun_2'],
		'koperasi'                       => $post['koperasi'],
		'ikm'                            => $post['ikm'],
		'brk'                            => $post['brk'],
		'bpjs_kesehatan'                 => $post['bpjs_kesehatan'],
		'biaya_adm_bank'                 => $post['biaya_adm_bank'],
		'gaji_netto'                     => $post['gaji_netto'],
		'prestasi'                       => $post['prestasi'],
		'jasa_medis'                     => $post['jasa_medis'],
		'contract_details'               => $post['contract_details'],
		'kontrak_isi'                    => $post['kontrak_isi'],
		'kontrak_ttd_karyawan'           => $post['kontrak_ttd_karyawan'],
		'kontrak_ttd_hrd'                => $post['kontrak_ttd_hrd'],
		'kontrak_nama_hrd'               => $post['kontrak_nama_hrd'],
		'kontrak_datetime_karyawan'      => $post['kontrak_datetime_karyawan'],
		'kontrak_datetime_hrd'           => $post['kontrak_datetime_hrd'],
		'no_sk'                          => $post['no_sk'],
		'tanggal_sk'                     => $post['tanggal_sk'],
		'tanggal_mutasi'                 => $post['tanggal_mutasi'],
		'prefix'                         => $post['prefix'],
		'alasan_resign'                  => $post['alasan_resign'],
		'status_berhenti_kerja'          => $post['status_berhenti_kerja'],
		'unit'                           => $post['unit'],
		'persen_jasmed_tarif_khusus'     => $post['persen_jasmed_tarif_khusus'],
		'spesialisasi'                   => $post['spesialisasi'],
	     ]);
			echo json_encode($res_add);
		}else{
			echo json_encode(
				array(
					'success' => false,
					'message' => 'Data Gagal Disimpan!'
				)
			);
		}
	}
  
	public function edit($params = null)
	{
	  $data['title']    = "Edit Master HR";
	  $data['contents'] = $this->_view . '/edit';
	  $data['detail'] = $this->master_hr->get(['id' => $this->input->get('id')])['data']['result'][0];
	  $data['js'] = [
		$this->_assets . 'qwerty_v1/js/master_v1/master_hr/edit.js',
	  ];
  
	  $this->load->view('qwerty_v1/layout/master', $data);
	}

	public function edit_process($params = null)
    {
		$this->form_validation->set_rules('no_rm', 'No RM', 'required');
		$this->form_validation->set_rules('profesi', 'Profesi', 'required');	
		$this->form_validation->set_rules('nomor_induk_karyawan', 'Nomor Induk Karyawan', 'required');	
		$this->form_validation->set_rules('bpjs_kode_dokter', 'BPJS Kode Dokter', 'required');
		$this->form_validation->set_rules('full_name', 'Full Name', 'required');	
		$this->form_validation->set_rules('username', 'Username', 'required');	
		$this->form_validation->set_rules('kode_karyawan_fingerprint', 'Kode Karyawan Fingerprint', 'required');
  
  
	if($this->form_validation->run() == TRUE){	

    $post = $this->input->post();
	$data = [
		'id'                             => $post['id'],
        'no_rm'                          => $post['no_rm'],
        'profesi'                        => $post['profesi'],
	    'profesi_jabatan'      			 => $post['profesi_jabatan'],	
		'role_prefix'          			 => $post['role_prefix'],	
		'full_name'           			 => $post['full_name'],	
		'username'             			 => $post['username'],	
		'pin'                  			 => $post['pin'],	
		'prefix_antrean'       			 => $post['prefix_antrean'],	
		'akses_navbar_level1'  			 => $post['akses_navbar_level1'],	
		'bpjs_kode_dokter'     			 => $post['bpjs_kode_dokter'],	
		'pin_renewal'          			 => $post['pin_renewal'],
		'job_start'            			 => $post['job_start'],
		'job_end'              			 => $post['job_end'],
		'tipe_jadwal'          			 => $post['tipe_jadwal'],
		'nomor_induk_karyawan' 			 => $post['nomor_induk_karyawan'],
		'kode_karyawan_fingerprint' 	 => $post['kode_karyawan_fingerprint'],
		'npwp'                      	 => $post['npwp'],
		'nama_bank'                 	 => $post['nama_bank'],
		'no_rekening'               	 => $post['no_rekening'],
		'no_ktp'                    	 => $post['no_ktp'],
		'nama_satuan_kerja'         	 => $post['nama_satuan_kerja'],
	    'eselon'                         => $post['eselon'],
		'golongan'                 		 => $post['golongan'],
		'nomor_induk_karyawan_pasangan'  => $post['nomor_induk_karyawan_pasangan'],
		'no_telp'                        => $post['no_telp'],
		'jumlah_anak'                    => $post['jumlah_anak'],
		'satusehat_id_practitioner'      => $post['satusehat_id_practitioner'],
		'status'                         => $post['status'],
		'akses'                          => $post['akses'],
		'gaji_bulanan'                   => $post['gaji_bulanan'],
		'str'                            => $post['str'],
		'str_exp_date'                   => $post['str_exp_date'],
		'sip'                            => $post['sip'],
		'sip_exp_date'                   => $post['sip_exp_date'],
		'ktk'                            => $post['ktk'],
		'group_unit_kerja'               => $post['group_unit_kerja'],
		'unit_kerja'                     => $post['unit_kerja'],
		'jenis_karyawan'                 => $post['jenis_karyawan'],
		'jabatan'                        => $post['jabatan'],
		'pendidikan'                     => $post['pendidikan'],
		'nama_universitas'               => $post['nama_universitas'],
		'status_karyawan'                => $post['status_karyawan'],
		'gaji_bruto'                     => $post['gaji_bruto'],
		'tunjangan_pulsa'                => $post['tunjangan_pulsa'], 
		'tunjangan_bbm'                  => $post['tunjangan_bbm'],
		'tunjangan_sekretaris'           => $post['tunjangan_sekretaris'],
		'tunjangan_makeup'               => $post['tunjangan_makeup'],
		'tunjangan_internal_memo'        => $post['tunjangan_internal_memo'],
		'tunjangan_mengelola_data'       => $post['tunjangan_mengelola_data'],
		'tunjangan_legal'                => $post['tunjangan_legal'],
		'tunjangan_marketing'            => $post['tunjangan_marketing'],
		'tunjangan_resiko'               => $post['tunjangan_resiko'],
		'tunjangan_sip'                  => $post['tunjangan_sip'],
		'tunjangan_casemix'              => $post['tunjangan_casemix'],
		'tunjangan_makan'                => $post['tunjangan_makan'],
		'tunjangan_transportasi'         => $post['tunjangan_transportasi'],
		'tunjangan_kinerja'              => $post['tunjangan_kinerja'],
		'tunjangan_lain_1'               => $post['tunjangan_lain_1'],
		'tunjangan_lain_2'               => $post['tunjangan_lain_2'],
		'jpk_4'                          => $post['jpk_4'],
		'jkk_jkn_054'                    => $post['jkk_jkn_054'],
		'jpk'                            => $post['jpk'],
		'jkk'                            => $post['jkk'],
		'jkm'                            => $post['jkm'],
		'gaji_total'                     => $post['gaji_total'],
		'biaya_jabatan'                  => $post['biaya_jabatan'],
		'iuran_jht'                      => $post['iuran_jht'],
		'iuran_jht_2'                    => $post['iuran_jht_2'],
		'pengurangan_total'              => $post['pengurangan_total'],
		'ph_netto_bulan'                 => $post['ph_netto_bulan'],
		'ph_netto_tahun'                 => $post['ph_netto_tahun'],   
		'ptkp'                           => $post['ptkp'],
		'pkp'                            => $post['pkp'],
		'pajak_5'                        => $post['pajak_5'],
		'pajak_15'                       => $post['pajak_15'],
		'pajak_25'                       => $post['pajak_25'],
		'pajak_30'                       => $post['pajak_30'],
		'pajak_35'                       => $post['pajak_35'],
		'pph_21_bulan'                   => $post['pph_21_bulan'],
		'pph_21_tahun'                   => $post['pph_21_tahun'],
		'pph'                            => $post['pph'],
		'gaji_bersih'                    => $post['gaji_bersih'],
		'bon_sementara'                  => $post['bon_sementara'],
		'seragam'                        => $post['seragam'],
		'jaminan_hari_tua'               => $post['jaminan_hari_tua'],
		'jaminan_hari_tua_2'             => $post['jaminan_hari_tua_2'],
		'rawat_inap'                     => $post['rawat_inap'],
		'apotek'                         => $post['apotek'],
		'jaminan_pensiun'                => $post['jaminan_pensiun'],
		'jaminan_pensiun_2'              => $post['jaminan_pensiun_2'],
		'koperasi'                       => $post['koperasi'],
		'ikm'                            => $post['ikm'],
		'brk'                            => $post['brk'],
		'bpjs_kesehatan'                 => $post['bpjs_kesehatan'],
		'biaya_adm_bank'                 => $post['biaya_adm_bank'],
		'gaji_netto'                     => $post['gaji_netto'],
		'prestasi'                       => $post['prestasi'],
		'jasa_medis'                     => $post['jasa_medis'],
		'contract_details'               => $post['contract_details'],
		'kontrak_isi'                    => $post['kontrak_isi'],
		'kontrak_ttd_karyawan'           => $post['kontrak_ttd_karyawan'],
		'kontrak_ttd_hrd'                => $post['kontrak_ttd_hrd'],
		'kontrak_nama_hrd'               => $post['kontrak_nama_hrd'],
		'kontrak_datetime_karyawan'      => $post['kontrak_datetime_karyawan'],
		'kontrak_datetime_hrd'           => $post['kontrak_datetime_hrd'],
		'no_sk'                          => $post['no_sk'],
		'tanggal_sk'                     => $post['tanggal_sk'],
		'tanggal_mutasi'                 => $post['tanggal_mutasi'],
		'prefix'                         => $post['prefix'],
		'alasan_resign'                  => $post['alasan_resign'],
		'status_berhenti_kerja'          => $post['status_berhenti_kerja'],
		'unit'                           => $post['unit'],
		'persen_jasmed_tarif_khusus'     => $post['persen_jasmed_tarif_khusus'],
		'spesialisasi'                   => $post['spesialisasi'],
	];
			$res_update  = $this->master_hr->update($data['id'], $data);
			echo json_encode($res_update);
		}
    }
  
	public function delete($params = null)
	{
	  $post = $this->input->post();
	  $res_delete = $this->master_hr->delete([
		'id'  => $post['id'],
	  ]);
	  echo json_encode($res_delete);
	}


}