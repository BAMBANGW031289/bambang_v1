<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_master_kamar extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->_class   = $this->uri->segments[1];
    $this->_view    = 'qwerty_v1/contents/master_v1/master_kamar';
    $this->load->model('qwerty_v1/master_v1/M_master_kamar', 'master_kamar');

    $this->id_kamar = 1;
  }


  public function index()
  {
    $data['title']    = "Master Kamar";
    $data['url']      = base_url($this->_class);
    $data['contents'] = $this->_view . '/index';
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_kamar/index.js',
    ];
    $data['css'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_kamar/style.css',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  function json()
  {
    $value = $this->_postvalue()['data'];
    $json = [
      'draw' => $this->input->post('draw'),
      'recordsTotal' => $value['record_total'],
      'recordsFiltered' => $value['record_filter']
    ];

    $data = [];

    $i = $_POST['start'];
    foreach ($value['result'] as $k => $v) {
      $i++;
      $row = [];
      $button = '<a href="' . base_url($this->_class . '/edit?id_kamar=' . $v['id_kamar']) . '" class="btn btn-sm btn-primary">Edit</a>';
      $button .= '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $v['id_kamar'] . '" style="margin-left:5px">Hapus</button>';
      $row[] = $i;
      $row[] = $button;
      $row[] = $v['perawatan'];
      $row[] = $v['kelas'];
      $row[] = $v['kode_siranap_kelas_perawatan'];

      $data[] = $row;
    }

    $json['data'] = $data;

    echo json_encode($json);
  }

  function _postvalue($paging = TRUE)
  {
    $params = [
      'offset' => $this->input->post('start'),
      'limit' => $this->input->post('length'),
      'search' => $this->input->post('search')['value'],
    ];


    if ($_POST['search']['value']) {
      $params['search'] = $_POST['search']['value'] ? $_POST['search']['value'] : '';
    }

    if (isset($_POST['ORDER'])) {
      if ($_POST['order']['0']['column']) {
        $params['order_column'] = $_POST['order']['0']['column'] ? $_POST['order']['0']['column'] : '2';
      }
      if ($_POST['order']['0']['dir']) {
        $params['order_dir'] = $_POST['order']['0']['dir'] ? $_POST['order']['0']['dir'] : 'asc';
      }
    }

    if (!$paging) {
      unset($params['offset']);
      unset($params['limit']);
    }

    $value = $this->master_kamar->get($params);
    return $value;
  }

  public function add($params = null)
  {
    $data['title']    = "Add Master Kamar";
    $data['contents'] = $this->_view . '/add';
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_kamar/add.js',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  public function add_process($params = null)
  {
    $post = $this->input->post();
    $res_add = $this->master_kamar->add([
      'id_departemen'                       => $post['id_departemen'],
      'kapasitas'                           => $post['kapasitas'], 
      'tersediapriawanita'                  => $post['tersediapriawanita'],
      'id_kamar_ref_kelas'                  => $post['id_kamar_ref_kelas'],
      'id_ref_siranap_kode_kelas_perawatan' => $post['id_ref_siranap_kode_kelas_perawatan'],
      'id_ref_siranap_kode_ruang_perawatan' => $post['id_ref_siranap_kode_ruang_perawatan'],
      'id_ref_rsonline_fasyankes'           => $post['id_ref_rsonline_fasyankes'],
      'perawatan'                           => $post['perawatan'],
      'kelas'                               => $post['kelas'],
      'kode_siranap_kelas_perawatan'        => $post['kode_siranap_kelas_perawatan'],
      'kode_siranap_ruang_perawatan'        => $post['kode_siranap_ruang_perawatan'],
      'dinkes_jenis_bed'                    => $post['kode_siranap_ruang_perawatan'],
      'tersediapria'                        => $post['tersediapria'],
      'tersediawanita'                      => $post['tersediawanita'],
      'is_del'                              => $post['is_del'],
      'del_date'                            => $post['del_date'],
      'del_by'                              => $post['del_by'],
      'id_user'                             => $post['id_user'],
      'is_bor'                              => $post['is_bor'], 
      'is_covid'                            => $post['is_covid'],
    ]);
    echo json_encode($res_add);
  }

  public function edit($params = null)
  {
    $data['title']    = "Edit Master Kamar";
    $data['contents'] = $this->_view . '/edit';
    $data['detail'] = $this->master_kamar->get(['id_kamar' => $this->input->get('id_kamar')])['data']['result'][0];
    $data['js'] = [
      $this->_assets . 'qwerty_v1/js/master_v1/master_kamar/edit.js',
    ];

    $this->load->view('qwerty_v1/layout/master', $data);
  }

  public function edit_process($params = null)
  {
    $post = $this->input->post();
    $res_update = $this->master_kamar->update([
      'id_kamar'                            => $post['id'],
      'id_departemen'                       => $post['id_departemen'],
      'kapasitas'                           => $post['kapasitas'],
      'tersediapriawanita'                  => $post['tersediapriawanita'],
      'id_kamar_ref_kelas'                  => $post['id_kamar_ref_kelas'],
      'id_ref_siranap_kode_kelas_perawatan' => $post['id_ref_siranap_kode_kelas_perawatan'],
      'id_ref_siranap_kode_ruang_perawatan' => $post['id_ref_siranap_kode_ruang_perawatan'],
      'id_ref_rsonline_fasyankes'           => $post['id_ref_rsonline_fasyankes'],
      'perawatan'                           => $post['perawatan'],
      'kelas'                               => $post['kelas'],
      'kode_siranap_kelas_perawatan'        => $post['kode_siranap_kelas_perawatan'],
      'kode_siranap_ruang_perawatan'        => $post['kode_siranap_ruang_perawatan'],
      'dinkes_jenis_bed'                    => $post['kode_siranap_ruang_perawatan'],
      'tersediapria'                        => $post['tersediapria'],
      'tersediawanita'                      => $post['tersediawanita'],
      'is_del'                              => $post['is_del'],
      'del_date'                            => $post['del_date'],
      'del_by'                              => $post['del_by'],
      'id_user'                             => $post['id_user'],
      'is_bor'                              => $post['is_bor'], 
      'is_covid'                            => $post['is_covid'],
    ]);
    echo json_encode($res_update);
  }

  public function delete($params = null)
  {
    $post = $this->input->post();
    $res_delete = $this->master_kamar->delete([
      'id'  => $post['id'],
    ]);
    echo json_encode($res_delete);
  }
}

/* End of file C_master_kamar.php */
