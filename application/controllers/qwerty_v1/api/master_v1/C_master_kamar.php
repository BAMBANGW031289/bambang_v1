<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class C_master_kamar extends RestController
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('qwerty_v1/master_v1/M_master_kamar', 'master_kamar');
    $this->id_kamar = 1;
  }


  public function index_get()
  {
    $get = $this->get();
    $res = $this->master_kamar->get($get);
    $this->response($res, $res['status']);
  }

  public function index_post()
  {
    $post = $this->post();
    $res  = $this->master_kamar->add($post);
    $this->response($res, $res['status']);
  }

  public function index_put($id = null)
  {
    $modelan = $this->master_kamar;
    $data = [
      'id_departemen'                       => $this->put('id_departemen'),
      'kapasitas'                           => $this->put('kapasitas'),
      'tersediapriawanita'                  => $this->put('tersediapriawanita'),
      'id_kamar_ref_kelas'                  => $this->put('id_kamar_ref_kelas'),
      'id_ref_siranap_kode_kelas_perawatan' => $this->put('id_ref_siranap_kode_kelas_perawatan'),
      'id_ref_siranap_kode_ruang_perawatan' => $this->put('id_ref_siranap_kode_ruang_perawatan'),
      'id_ref_rsonline_fasyankes'           => $this->put('id_ref_rsonline_fasyankes'),
      'perawatan'                           => $this->put('perawatan'),
      'kelas'                               => $this->put('kelas'),
      'kode_siranap_kelas_perawatan'        => $this->put('kode_siranap_kelas_perawatan'),
      'kode_siranap_ruang_perawatan'        => $this->put('kode_siranap_ruang_perawatan'),
      'dinkes_jenis_bed'                    => $this->put('dinkes_jenis_bed'),
      'tersediapria'                        => $this->put('tersediapria'),
      'tersediawanita'                      => $this->put('tersediawanita'),
      'is_del'                              => $this->put('is_del'),
      'del_date'                            => $this->put('del_date'),
      'del_by'                              => $this->put('del_by'),
      'id_user'                             => $this->put('id_user'),
      'is_bor'                              => $this->put('is_bor'), 
      'is_covid'                            => $this->put('is_covid'),
    ];
    $res  = $modelan->update($id, $data);
    $this->response($res, $res['status']);
  }

  public function index_delete($params = null)
  {
    $res = $this->master_kamar->delete([
      'id' => $params
    ]);
    $this->response($res, $res['status']);
  }
}

/* End of file C_master_kamar_v1.php */
