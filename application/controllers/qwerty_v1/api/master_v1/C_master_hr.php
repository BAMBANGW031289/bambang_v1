<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class C_master_hr extends RestController
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('qwerty_v1/master_v1/M_master_hr', 'master_hr');
    $this->id = 1;
  }


  public function index_get()
  {
    $get = $this->get();
    $res = $this->master_hr->get($get);
    $this->response($res, $res['status']);
  }

  public function index_post()
  {
    $post = $this->post();
    $res  = $this->master_hr->add($post);
    $this->response($res, $res['status']);
  }

  public function index_put($id = null)
  {
    $modelan = $this->master_hr;
    $data = [
      'no_rm'                          => $this->put('no_rm'),
      'profesi'                        => $this->put('profesi'),
      'profesi_jabatan'      			     => $this->put('profesi_jabatan'),
      'role_prefix'          			     => $this->put('role_prefix'),
      'full_name'           			     => $this->put('full_name'),
      'username'             			     => $this->put('username'),	
      'pin'                  			     => $this->put('pin'),
      'prefix_antrean'       			     => $this->put('prefix_antrean'),	
      'akses_navbar_level1'  			     => $this->put('akses_navbar_level1'),
      'bpjs_kode_dokter'     			     => $this->put('bpjs_kode_dokter'),	
      'pin_renewal'          			     => $this->put('pin_renewal'),
      'job_start'            			     => $this->put('job_start'),
      'job_end'              			     => $this->put('job_end'),
      'tipe_jadwal'          			     => $this->put('tipe_jadwal'),
      'nomor_induk_karyawan' 			     => $this->put('nomor_induk_karyawan'),
      'kode_karyawan_fingerprint' 	   => $this->put('kode_karyawan_fingerprint'),
      'npwp'                      	   => $this->put('npwp'),
      'nama_bank'                 	   => $this->put('nama_bank'),
      'no_rekening'               	   => $this->put('no_rekening'),
      'no_ktp'                    	   => $this->put('no_ktp'),
      'nama_satuan_kerja'         	   => $this->put('nama_satuan_kerja'),
      'eselon'                         => $this->put('eselon'),
      'golongan'                 		   => $this->put('golongan'),
      'nomor_induk_karyawan_pasangan'  => $this->put('nomor_induk_karyawan_pasangan'),
      'no_telp'                        => $this->put('no_telp'),
      'jumlah_anak'                    => $this->put('jumlah_anak'),
      'satusehat_id_practitioner'      => $this->put('satusehat_id_practitioner'),
      'status'                         => $this->put('status'),
      'akses'                          => $this->put('akses'),
      'gaji_bulanan'                   => $this->put('gaji_bulanan'),
      'str'                            => $this->put('str'),
      'str_exp_date'                   => $this->put('str_exp_date'),
      'sip'                            => $this->put('sip'),
      'sip_exp_date'                   => $this->put('sip_exp_date'),
      'ktk'                            => $this->put('ktk'),
      'group_unit_kerja'               => $this->put('group_unit_kerja'),
      'unit_kerja'                     => $this->put('unit_kerja'),
      'jenis_karyawan'                 => $this->put('jenis_karyawan'),
      'jabatan'                        => $this->put('jabatan'),
      'pendidikan'                     => $this->put('pendidikan'),
      'nama_universitas'               => $this->put('nama_universitas'),
      'status_karyawan'                => $this->put('status_karyawan'),
      'gaji_bruto'                     => $this->put('gaji_bruto'),
      'tunjangan_pulsa'                => $this->put('tunjangan_pulsa'),
      'tunjangan_bbm'                  => $this->put('tunjangan_bbm'),
      'tunjangan_sekretaris'           => $this->put('tunjangan_sekretaris'),
      'tunjangan_makeup'               => $this->put('tunjangan_makeup'),
      'tunjangan_internal_memo'        => $this->put('tunjangan_internal_memo'),
      'tunjangan_mengelola_data'       => $this->put('tunjangan_mengelola_data'),
      'tunjangan_legal'                => $this->put('tunjangan_legal'),
      'tunjangan_marketing'            => $this->put('tunjangan_marketing'),
      'tunjangan_resiko'               => $this->put('tunjangan_resiko'),
      'tunjangan_sip'                  => $this->put('tunjangan_sip'),
      'tunjangan_casemix'              => $this->put('tunjangan_casemix'),
      'tunjangan_makan'                => $this->put('tunjangan_makan'),
      'tunjangan_transportasi'         => $this->put('tunjangan_transportasi'),
      'tunjangan_kinerja'              => $this->put('tunjangan_kinerja'),
      'tunjangan_lain_1'               => $this->put('tunjangan_lain_1'),
      'tunjangan_lain_2'               => $this->put('tunjangan_lain_2'),
      'jpk_4'                          => $this->put('jpk_4'),
      'jkk_jkn_054'                    => $this->put('jkk_jkn_054'),
      'jpk'                            => $this->put('jpk'),
      'jkk'                            => $this->put('jkk'),
      'jkm'                            => $this->put('jkm'),
      'gaji_total'                     => $this->put('gaji_total'),
      'biaya_jabatan'                  => $this->put('biaya_jabatan'),
      'iuran_jht'                      => $this->put('iuran_jht'),
      'iuran_jht_2'                    => $this->put('iuran_jht_2'),
      'pengurangan_total'              => $this->put('pengurangan_total'),
      'ph_netto_bulan'                 => $this->put('ph_netto_bulan'),
      'ph_netto_tahun'                 => $this->put('ph_netto_tahun'),   
      'ptkp'                           => $this->put('ptkp'),
      'pkp'                            => $this->put('pkp'),
      'pajak_5'                        => $this->put('pajak_5'),
      'pajak_15'                       => $this->put('pajak_15'),
      'pajak_25'                       => $this->put('pajak_25'),
      'pajak_30'                       => $this->put('pajak_30'),
      'pajak_35'                       => $this->put('pajak_35'),
      'pph_21_bulan'                   => $this->put('pph_21_bulan'),
      'pph_21_tahun'                   => $this->put('pph_21_tahun'),
      'pph'                            => $this->put('pph'),
      'gaji_bersih'                    => $this->put('gaji_bersih'),
      'bon_sementara'                  => $this->put('bon_sementara'),
      'seragam'                        => $this->put('seragam'),
      'jaminan_hari_tua'               => $this->put('jaminan_hari_tua'),
      'jaminan_hari_tua_2'             => $this->put('jaminan_hari_tua_2'),
      'rawat_inap'                     => $this->put('rawat_inap'),
      'apotek'                         => $this->put('apotek'),
      'jaminan_pensiun'                => $this->put('jaminan_pensiun'),
      'jaminan_pensiun_2'              => $this->put('jaminan_pensiun_2'),
      'koperasi'                       => $this->put('koperasi'),
      'ikm'                            => $this->put('ikm'),
      'brk'                            => $this->put('brk'),
      'bpjs_kesehatan'                 => $this->put('bpjs_kesehatan'),
      'biaya_adm_bank'                 => $this->put('biaya_adm_bank'),
      'gaji_netto'                     => $this->put('gaji_netto'),
      'prestasi'                       => $this->put('prestasi'),
      'jasa_medis'                     => $this->put('jasa_medis'),
      'contract_details'               => $this->put('contract_details'),
      'kontrak_isi'                    => $this->put('kontrak_isi'),
      'kontrak_ttd_karyawan'           => $this->put('kontrak_ttd_karyawan'),
      'kontrak_ttd_hrd'                => $this->put('kontrak_ttd_hrd'),
      'kontrak_nama_hrd'               => $this->put('kontrak_nama_hrd'),
      'kontrak_datetime_karyawan'      => $this->put('kontrak_datetime_karyawan'),
      'kontrak_datetime_hrd'           => $this->put('kontrak_datetime_hrd'),
      'no_sk'                          => $this->put('no_sk'),
      'tanggal_sk'                     => $this->put('tanggal_sk'),
      'tanggal_mutasi'                 => $this->put('tanggal_mutasi'),
      'prefix'                         => $this->put('prefix'),
      'alasan_resign'                  => $this->put('alasan_resign'),
      'status_berhenti_kerja'          => $this->put('status_berhenti_kerja'),
      'unit'                           => $this->put('unit'),
      'persen_jasmed_tarif_khusus'     => $this->put('persen_jasmed_tarif_khusus'),
      'spesialisasi'                   => $this->put('spesialisasi'),
    ];
    $res  = $modelan->update($id, $data);
    $this->response($res, $res['status']);
  }

  public function index_delete($params = null)
  {
    $res = $this->master_hr->delete([
      'id' => $params
    ]);
    $this->response($res, $res['status']);
  }
}

/* End of file C_master_hr_v1.php */
