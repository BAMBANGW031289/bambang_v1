<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class C_master_kartu_keluarga extends RestController
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('qwerty_v1/master_v1/M_master_kartu_keluarga', 'master_kartu_keluarga');
    //$this->id_ref_master_tipe = 82;
  }


  public function index_get()
  {
    $get = $this->get();
    $res = $this->master_kartu_keluarga->get($get);
    $this->response($res, $res['status']);
  }

  public function index_post()
  {
    $post = $this->post();
    $res  = $this->master_kartu_keluarga->add($post);
    $this->response($res, $res['status']);
  }

  public function index_put($id = null)
  {
    $modelan = $this->master_kartu_keluarga;
    $data = [
      'id_users_profile'       => $this->put('id_users_profile'),
      'nomor_kartu_keluarga'   => $this->put('nomor_kartu_keluarga'),
      'alamat_rumah'           => $this->put('alamat_rumah'),
      'kode_pos'               => $this->put('kode_pos'),
      'created_date'           => $this->put('created_date'),
      'jakarta_bandung'        => $this->put('jakarta_bandung'),
      'simpat_proxl'           => $this->put('simpat_proxl'),
    ];
    $res  = $modelan->update($id, $data);
    $this->response($res, $res['status']);
  }

  public function index_delete($params = null)
  {
    $res = $this->master_kartu_keluarga->delete([
      'id' => $params
    ]);
    $this->response($res, $res['status']);
  }
}

/* End of file C_master_kartu_keluarga_v1.php */
