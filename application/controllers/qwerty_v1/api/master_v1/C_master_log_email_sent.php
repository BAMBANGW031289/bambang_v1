<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class C_master_log_email_sent extends RestController
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('qwerty_v1/master_v1/M_master_log_email_sent', 'master_log_email_sent');
    //$this->id_ref_master_tipe = 82;
  }


  public function index_get()
  {
    $get = $this->get();
    $res = $this->master_log_email_sent->get($get);
    $this->response($res, $res['status']);
  }

  public function index_post()
  {
    $post = $this->post();
    $res  = $this->master_log_email_sent->add($post);
    $this->response($res, $res['status']);
  }

  public function index_put($id = null)
  {
    $modelan = $this->master_log_email_sent;
    $data = [
      'tipe_email_sent' => $this->put('tipe_email_sent'),
      'no_ref'          => $this->put('no_ref'),
      'email_sent_to'   => $this->put('email_sent_to'),
      'created_date'    => date("Y-m-d h:i:sa"),
      'created_by'      => "admin",
    ];
    $res  = $modelan->update($id, $data);
    $this->response($res, $res['status']);
  }

  public function index_delete($params = null)
  {
    $res = $this->master_log_email_sent->delete([
      'id' => $params
    ]);
    $this->response($res, $res['status']);
  }
}

/* End of file C_master_log_email_sent_v1.php */
